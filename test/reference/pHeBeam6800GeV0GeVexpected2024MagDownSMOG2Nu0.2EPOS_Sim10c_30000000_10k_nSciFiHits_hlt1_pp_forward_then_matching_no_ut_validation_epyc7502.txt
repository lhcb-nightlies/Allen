forward_validator validation:
TrackChecker output                               :         0/      177   0.00% ghosts
for P>3GeV,Pt>0.5GeV                              :         0/      177   0.00% ghosts
01_long                                           :       171/     2416   7.08% (  7.13%),         3 (  1.72%) clones, pur  99.87%, hit eff  98.55%
02_long_P>5GeV                                    :       171/     1602  10.67% ( 10.34%),         3 (  1.72%) clones, pur  99.87%, hit eff  98.55%
03_long_strange                                   :         2/      100   2.00% (  2.27%),         0 (  0.00%) clones, pur 100.00%, hit eff  95.45%
04_long_strange_P>5GeV                            :         2/       54   3.70% (  3.49%),         0 (  0.00%) clones, pur 100.00%, hit eff  95.45%
07_long_electrons                                 :         0/      315   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
08_long_electrons_P>5GeV                          :         0/      178   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
long_P>5GeV_AND_Pt>1GeV                           :       100/      108  92.59% ( 94.60%),         2 (  1.96%) clones, pur  99.78%, hit eff  99.85%
11_noVelo_UT                                      :         0/      329   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
12_noVelo_UT_P>5GeV                               :         0/      133   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
13_long_PT>2GeV                                   :         8/        8 100.00% (100.00%),         0 (  0.00%) clones, pur 100.00%, hit eff 100.00%
15_long_strange_P>5GeV                            :         2/       54   3.70% (  3.49%),         0 (  0.00%) clones, pur 100.00%, hit eff  95.45%
16_long_strange_P>5GeV_PT>500MeV                  :         2/       13  15.38% ( 15.38%),         0 (  0.00%) clones, pur 100.00%, hit eff  95.45%
18_long_nSciFiHits_gt_0_AND_lt_5000               :       171/     2731   6.26% (  6.36%),         3 (  1.72%) clones, pur  99.87%, hit eff  98.55%


long_validator validation:
TrackChecker output                               :        38/     1999   1.90% ghosts
for P>3GeV,Pt>0.5GeV                              :         0/      657   0.00% ghosts
01_long                                           :      1704/     2416  70.53% ( 70.48%),        41 (  2.35%) clones, pur  99.93%, hit eff  98.97%
02_long_P>5GeV                                    :      1394/     1602  87.02% ( 87.62%),        36 (  2.52%) clones, pur  99.95%, hit eff  99.28%
03_long_strange                                   :        62/      100  62.00% ( 61.49%),         2 (  3.12%) clones, pur  99.85%, hit eff  99.57%
04_long_strange_P>5GeV                            :        49/       54  90.74% ( 88.37%),         2 (  3.92%) clones, pur  99.94%, hit eff  99.35%
07_long_electrons                                 :        98/      315  31.11% ( 30.99%),         2 (  2.00%) clones, pur  99.49%, hit eff  99.07%
08_long_electrons_P>5GeV                          :        85/      178  47.75% ( 48.19%),         2 (  2.30%) clones, pur  99.41%, hit eff  99.42%
long_P>5GeV_AND_Pt>1GeV                           :       101/      108  93.52% ( 95.70%),         2 (  1.94%) clones, pur  99.78%, hit eff  99.85%
11_noVelo_UT                                      :         0/      329   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
12_noVelo_UT_P>5GeV                               :         0/      133   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
13_long_PT>2GeV                                   :         8/        8 100.00% (100.00%),         0 (  0.00%) clones, pur 100.00%, hit eff 100.00%
15_long_strange_P>5GeV                            :        49/       54  90.74% ( 88.37%),         2 (  3.92%) clones, pur  99.94%, hit eff  99.35%
16_long_strange_P>5GeV_PT>500MeV                  :        12/       13  92.31% ( 92.31%),         1 (  7.69%) clones, pur  99.77%, hit eff  98.02%
18_long_nSciFiHits_gt_0_AND_lt_5000               :      1802/     2731  65.98% ( 66.10%),        43 (  2.33%) clones, pur  99.91%, hit eff  98.97%


muon_validator validation:
Muon fraction in all MCPs:                                                452/    37439   0.01% 
Muon fraction in MCPs to which a track(s) was matched:                     18/     2230   0.01% 
Correctly identified muons with isMuon:                                    15/       18  83.33% 
Correctly identified muons from strange decays with isMuon:                 0/        0   -nan% 
Correctly identified muons from B decays with isMuon:                       0/        0   -nan% 
Tracks identified as muon with isMuon, but matched to non-muon MCP:        54/     2212   2.44% 
Ghost tracks identified as muon with isMuon:                                0/       38   0.00% 


pv_validator validation:
REC and MC vertices matched by dz distance
MC PV is reconstructible if at least 4 tracks are reconstructed
MC PV is isolated if dz to closest reconstructible MC PV > 10.00 mm
REC and MC vertices matched by dz distance

All                  :  0.926 (   363/   392)
Isolated             :  0.935 (   357/   382)
Close                :  0.600 (     6/    10)
False rate           :  0.022 (     8/   371)
Real false rate      :  0.022 (     8/   371)
Clones               :  0.000 (     0/   363)


rate_validator validation:
Hlt1BGIPseudoPVsBeamOne:                            4/   500, (  240.00 +/-   119.52) kHz
Hlt1BGIPseudoPVsBeamTwo:                            0/   500, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsDownBeamBeam:                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsIRBeamBeam:                         0/   500, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsNoBeam:                             0/   500, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsUpBeamBeam:                         0/   500, (    0.00 +/-     0.00) kHz
Hlt1BeamGas:                                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1ConeJet100GeV:                                  0/   500, (    0.00 +/-     0.00) kHz
Hlt1ConeJet15GeV:                                   0/   500, (    0.00 +/-     0.00) kHz
Hlt1ConeJet30GeV:                                   0/   500, (    0.00 +/-     0.00) kHz
Hlt1ConeJet50GeV:                                   0/   500, (    0.00 +/-     0.00) kHz
Hlt1D2KK:                                           0/   500, (    0.00 +/-     0.00) kHz
Hlt1D2KPi:                                          0/   500, (    0.00 +/-     0.00) kHz
Hlt1D2KPiAlignment:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1D2Kshh:                                         0/   500, (    0.00 +/-     0.00) kHz
Hlt1D2PiPi:                                         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DetJpsiToMuMuNegTagLine:                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1DetJpsiToMuMuPosTagLine:                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronDisplaced:                            0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronHighMass:                             0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronHighMass_SS:                          0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice1_displaced:      0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice1_prompt:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice2_displaced:      0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice2_prompt:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice3_displaced:      0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice3_prompt:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice4_displaced:      0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice4_prompt:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice1_displaced:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice1_prompt:            0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice2_displaced:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice2_prompt:            0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice3_displaced:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice3_prompt:            0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice4_displaced:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice4_prompt:            0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronSoft:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDisplaced:                                0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_SS:                              0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_VLowMass:                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_VLowMass_SS:                     0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonHighMass:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonJpsiMassAlignment:                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonNoIP:                                     0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonNoIP_SS:                                  0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonSoft:                                     0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiPhotonHighMass:                               0/   500, (    0.00 +/-     0.00) kHz
Hlt1Dst2D0Pi:                                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1Dst2D0PiAlignment:                              0/   500, (    0.00 +/-     0.00) kHz
Hlt1ErrorBank:                                      0/   500, (    0.00 +/-     0.00) kHz
Hlt1GECPassthrough:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1KsToPiPi:                                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1KsToPiPiDoubleMuonMisID:                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1LambdaLLDetachedTrack:                          0/   500, (    0.00 +/-     0.00) kHz
Hlt1MaterialVertexSeedsDownstreamz:                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1MaterialVertexSeeds_DWFS:                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1ODIN1kHzLumi:                                   0/   500, (    0.00 +/-     0.00) kHz
Hlt1ODINCalib:                                      0/   500, (    0.00 +/-     0.00) kHz
Hlt1ODINLumi:                                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1ODINeeFarFromActivity:                          0/   500, (    0.00 +/-     0.00) kHz
Hlt1OneMuonTrackLine:                               0/   500, (    0.00 +/-     0.00) kHz
Hlt1Passthrough:                                    0/   500, (    0.00 +/-     0.00) kHz
Hlt1PassthroughPVinSMOG2:                           0/   500, (    0.00 +/-     0.00) kHz
Hlt1Pi02GammaGamma:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1RICH1Alignment:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1RICH2Alignment:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG22BodyGeneric:                              0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG22BodyGenericPrompt:                        1/   500, (   60.00 +/-    59.94) kHz
Hlt1SMOG2BELowMultElectrons:                        2/   500, (  120.00 +/-    84.68) kHz
Hlt1SMOG2BENoBias:                                  0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2D2Kpi:                                     0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2DiMuonHighMass:                            0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2DisplacedDiMuon:                           0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2JPsiToMuMuTaP_NegTag:                      0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2JPsiToMuMuTaP_PosTag:                      0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2KsTopipi:                                  1/   500, (   60.00 +/-    59.94) kHz
Hlt1SMOG2L0Toppi:                                   1/   500, (   60.00 +/-    59.94) kHz
Hlt1SMOG2MinimumBias:                               0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2PassThroughLowMult5:                      10/   500, (  600.00 +/-   187.83) kHz
Hlt1SMOG2SingleMuon:                                1/   500, (   60.00 +/-    59.94) kHz
Hlt1SMOG2SingleTrackHighPt:                         0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2SingleTrackVeryHighPt:                     0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2etacTopp:                                  1/   500, (   60.00 +/-    59.94) kHz
Hlt1SingleHighPtElectron:                           0/   500, (    0.00 +/-     0.00) kHz
Hlt1SingleHighPtMuon:                               0/   500, (    0.00 +/-     0.00) kHz
Hlt1SingleHighPtMuonNoMuID:                         0/   500, (    0.00 +/-     0.00) kHz
Hlt1TAEPassthrough:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1TrackElectronMVA:                               0/   500, (    0.00 +/-     0.00) kHz
Hlt1TrackMVA:                                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1TrackMuonMVA:                                   0/   500, (    0.00 +/-     0.00) kHz
Hlt1TwoKs:                                          0/   500, (    0.00 +/-     0.00) kHz
Hlt1TwoTrackKs:                                     0/   500, (    0.00 +/-     0.00) kHz
Hlt1TwoTrackMVA:                                    0/   500, (    0.00 +/-     0.00) kHz
Hlt1UpsilonAlignment:                               0/   500, (    0.00 +/-     0.00) kHz
Hlt1VeloMicroBias:                                  0/   500, (    0.00 +/-     0.00) kHz
Hlt1VeloMicroBiasVeloClosing:                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1XiOmegaLLL:                                     0/   500, (    0.00 +/-     0.00) kHz
Inclusive:                                         19/   500, ( 1140.00 +/-   256.52) kHz


seed_validator validation:
TrackChecker output                               :         6/     3440   0.17% ghosts
for P>3GeV,Pt>0.5GeV                              :         0/        0   -nan% ghosts
00_P>3Gev_Pt>0.5                                  :       495/      716  69.13% ( 69.50%),         0 (  0.00%) clones, pur  99.91%, hit eff  98.81%
01_long                                           :      1798/     2416  74.42% ( 74.16%),         5 (  0.28%) clones, pur  99.88%, hit eff  98.82%
---1. phi quadrant                                :       458/      618  74.11% ( 77.19%),         2 (  0.43%) clones, pur  99.82%, hit eff  98.74%
---2. phi quadrant                                :       449/      589  76.23% ( 74.40%),         1 (  0.22%) clones, pur  99.89%, hit eff  98.56%
---3. phi quadrant                                :       457/      635  71.97% ( 73.16%),         1 (  0.22%) clones, pur  99.90%, hit eff  99.01%
---4. phi quadrant                                :       434/      574  75.61% ( 73.98%),         1 (  0.23%) clones, pur  99.91%, hit eff  98.97%
---eta < 2.5, small x, large y                    :         2/       27   7.41% (  9.09%),         0 (  0.00%) clones, pur 100.00%, hit eff  95.83%
---eta < 2.5, large x, small y                    :        11/       45  24.44% ( 24.36%),         0 (  0.00%) clones, pur 100.00%, hit eff  96.07%
---eta > 2.5, small x, large y                    :       662/      878  75.40% ( 74.69%),         5 (  0.75%) clones, pur  99.77%, hit eff  98.52%
---eta > 2.5, large x, small y                    :      1123/     1466  76.60% ( 77.22%),         0 (  0.00%) clones, pur  99.94%, hit eff  99.03%
02_long_P>5GeV                                    :      1387/     1602  86.58% ( 86.90%),         4 (  0.29%) clones, pur  99.89%, hit eff  99.23%
02_long_P>5GeV, eta > 4                           :       798/      903  88.37% ( 89.47%),         3 (  0.37%) clones, pur  99.90%, hit eff  99.23%
---eta < 2.5, small x, large y                    :         1/        5  20.00% ( 20.00%),         0 (  0.00%) clones, pur 100.00%, hit eff  91.67%
---eta < 2.5, large x, small y                    :         2/        6  33.33% ( 33.33%),         0 (  0.00%) clones, pur 100.00%, hit eff 100.00%
---eta > 2.5, small x, large y                    :       536/      609  88.01% ( 87.78%),         4 (  0.74%) clones, pur  99.78%, hit eff  99.14%
---eta > 2.5, large x, small y                    :       848/      982  86.35% ( 87.11%),         0 (  0.00%) clones, pur  99.96%, hit eff  99.29%
03_long_P>3GeV                                    :      1798/     2113  85.09% ( 84.78%),         5 (  0.28%) clones, pur  99.88%, hit eff  98.82%
04_long_P>0.5GeV                                  :      1798/     2416  74.42% ( 74.16%),         5 (  0.28%) clones, pur  99.88%, hit eff  98.82%
08_UT+SciFi                                       :       220/      449  49.00% ( 45.39%),         1 (  0.45%) clones, pur  99.91%, hit eff  97.24%
09_UT+SciFi_P>5GeV                                :       136/      148  91.89% ( 93.05%),         1 (  0.73%) clones, pur 100.00%, hit eff  98.33%
10_UT+SciFi_P>3GeV                                :       220/      288  76.39% ( 74.80%),         1 (  0.45%) clones, pur  99.91%, hit eff  97.24%
11_UT+SciFi_fromStrange                           :        42/       54  77.78% ( 75.23%),         1 (  2.33%) clones, pur 100.00%, hit eff  97.93%
12_UT+SciFi_fromStrange_P>5GeV                    :        29/       30  96.67% ( 98.00%),         1 (  3.33%) clones, pur 100.00%, hit eff  97.86%
13_UT+SciFi_fromStrange_P>3GeV                    :        42/       45  93.33% ( 92.19%),         1 (  2.33%) clones, pur 100.00%, hit eff  97.93%
14_long_electrons                                 :       154/      315  48.89% ( 45.68%),         2 (  1.28%) clones, pur  99.81%, hit eff  98.81%
15_long_electrons_P>5GeV                          :       128/      178  71.91% ( 70.90%),         1 (  0.78%) clones, pur  99.84%, hit eff  99.15%
16_long_electrons_P>3GeV                          :       154/      255  60.39% ( 57.97%),         2 (  1.28%) clones, pur  99.81%, hit eff  98.81%
19_long_PT>2GeV                                   :         0/        8   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
21_long_strange_P>5GeV                            :        51/       54  94.44% ( 94.19%),         0 (  0.00%) clones, pur  99.82%, hit eff  99.00%
22_long_strange_P>5GeV_PT>500MeV                  :        11/       13  84.62% ( 84.62%),         0 (  0.00%) clones, pur 100.00%, hit eff  96.97%
24_noVelo+UT+T_fromKs0                            :        34/       45  75.56% ( 75.81%),         1 (  2.86%) clones, pur 100.00%, hit eff  98.17%
25_noVelo+UT+T_fromLambda                         :         5/        8  62.50% ( 64.29%),         0 (  0.00%) clones, pur 100.00%, hit eff  98.33%
27_noVelo+UT+T_fromKs0_P>5GeV                     :        21/       22  95.45% ( 97.37%),         1 (  4.55%) clones, pur 100.00%, hit eff  98.22%
28_noVelo+UT+T_fromLambda_P>5GeV                  :         3/        3 100.00% (100.00%),         0 (  0.00%) clones, pur 100.00%, hit eff 100.00%
30_noVelo+UT+T_fromKs0_P>5GeV_PT>500MeV           :         7/        7 100.00% (100.00%),         0 (  0.00%) clones, pur 100.00%, hit eff 100.00%
31_noVelo+UT+T_fromLambda_P>5GeV_PT>500MeV        :         3/        3 100.00% (100.00%),         0 (  0.00%) clones, pur 100.00%, hit eff 100.00%
35_long_nSciFiHits_gt_0_AND_lt_5000               :      2358/     3225  73.12% ( 73.86%),         7 (  0.30%) clones, pur  99.88%, hit eff  98.94%


seed_xz_validator validation:
TrackChecker output                               :        38/     4154   0.91% ghosts
for P>3GeV,Pt>0.5GeV                              :         0/        0   -nan% ghosts
00_P>3Gev_Pt>0.5                                  :       697/      716  97.35% ( 97.48%),         0 (  0.00%) clones, pur  99.87%, hit eff  49.87%
01_long                                           :      2037/     2416  84.31% ( 84.64%),         3 (  0.15%) clones, pur  99.90%, hit eff  49.83%
---1. phi quadrant                                :       526/      618  85.11% ( 87.80%),         1 (  0.19%) clones, pur  99.86%, hit eff  49.76%
---2. phi quadrant                                :       498/      589  84.55% ( 83.00%),         1 (  0.20%) clones, pur  99.85%, hit eff  49.74%
---3. phi quadrant                                :       523/      635  82.36% ( 83.74%),         1 (  0.19%) clones, pur  99.93%, hit eff  49.83%
---4. phi quadrant                                :       490/      574  85.37% ( 84.81%),         0 (  0.00%) clones, pur  99.97%, hit eff  49.98%
---eta < 2.5, small x, large y                    :         7/       27  25.93% ( 25.00%),         0 (  0.00%) clones, pur  97.14%, hit eff  48.92%
---eta < 2.5, large x, small y                    :        16/       45  35.56% ( 34.19%),         0 (  0.00%) clones, pur 100.00%, hit eff  50.00%
---eta > 2.5, small x, large y                    :       758/      878  86.33% ( 87.11%),         3 (  0.39%) clones, pur  99.83%, hit eff  49.82%
---eta > 2.5, large x, small y                    :      1256/     1466  85.68% ( 85.80%),         0 (  0.00%) clones, pur  99.96%, hit eff  49.83%
02_long_P>5GeV                                    :      1581/     1602  98.69% ( 98.77%),         2 (  0.13%) clones, pur  99.89%, hit eff  49.83%
02_long_P>5GeV, eta > 4                           :       891/      903  98.67% ( 98.69%),         2 (  0.22%) clones, pur  99.93%, hit eff  49.81%
---eta < 2.5, small x, large y                    :         5/        5 100.00% (100.00%),         0 (  0.00%) clones, pur  96.00%, hit eff  48.48%
---eta < 2.5, large x, small y                    :         6/        6 100.00% (100.00%),         0 (  0.00%) clones, pur 100.00%, hit eff  50.00%
---eta > 2.5, small x, large y                    :       599/      609  98.36% ( 98.48%),         2 (  0.33%) clones, pur  99.84%, hit eff  49.83%
---eta > 2.5, large x, small y                    :       971/      982  98.88% ( 98.99%),         0 (  0.00%) clones, pur  99.95%, hit eff  49.83%
03_long_P>3GeV                                    :      2037/     2113  96.40% ( 96.56%),         3 (  0.15%) clones, pur  99.90%, hit eff  49.83%
04_long_P>0.5GeV                                  :      2037/     2416  84.31% ( 84.64%),         3 (  0.15%) clones, pur  99.90%, hit eff  49.83%
08_UT+SciFi                                       :       254/      449  56.57% ( 53.18%),         1 (  0.39%) clones, pur  99.93%, hit eff  49.74%
09_UT+SciFi_P>5GeV                                :       146/      148  98.65% ( 99.30%),         1 (  0.68%) clones, pur 100.00%, hit eff  49.77%
10_UT+SciFi_P>3GeV                                :       253/      288  87.85% ( 86.90%),         1 (  0.39%) clones, pur  99.93%, hit eff  49.74%
11_UT+SciFi_fromStrange                           :        43/       54  79.63% ( 76.58%),         1 (  2.27%) clones, pur 100.00%, hit eff  50.27%
12_UT+SciFi_fromStrange_P>5GeV                    :        30/       30 100.00% (100.00%),         1 (  3.23%) clones, pur 100.00%, hit eff  49.94%
13_UT+SciFi_fromStrange_P>3GeV                    :        43/       45  95.56% ( 93.75%),         1 (  2.27%) clones, pur 100.00%, hit eff  50.27%
14_long_electrons                                 :       157/      315  49.84% ( 46.68%),         1 (  0.63%) clones, pur  99.62%, hit eff  49.56%
15_long_electrons_P>5GeV                          :       131/      178  73.60% ( 72.40%),         0 (  0.00%) clones, pur  99.69%, hit eff  49.67%
16_long_electrons_P>3GeV                          :       157/      255  61.57% ( 59.13%),         1 (  0.63%) clones, pur  99.62%, hit eff  49.56%
19_long_PT>2GeV                                   :         8/        8 100.00% (100.00%),         0 (  0.00%) clones, pur 100.00%, hit eff  50.57%
21_long_strange_P>5GeV                            :        53/       54  98.15% ( 97.67%),         0 (  0.00%) clones, pur 100.00%, hit eff  49.34%
22_long_strange_P>5GeV_PT>500MeV                  :        13/       13 100.00% (100.00%),         0 (  0.00%) clones, pur 100.00%, hit eff  48.37%
24_noVelo+UT+T_fromKs0                            :        35/       45  77.78% ( 76.88%),         1 (  2.78%) clones, pur 100.00%, hit eff  49.85%
25_noVelo+UT+T_fromLambda                         :         5/        8  62.50% ( 64.29%),         0 (  0.00%) clones, pur 100.00%, hit eff  50.00%
27_noVelo+UT+T_fromKs0_P>5GeV                     :        22/       22 100.00% (100.00%),         1 (  4.35%) clones, pur 100.00%, hit eff  49.73%
28_noVelo+UT+T_fromLambda_P>5GeV                  :         3/        3 100.00% (100.00%),         0 (  0.00%) clones, pur 100.00%, hit eff  50.00%
30_noVelo+UT+T_fromKs0_P>5GeV_PT>500MeV           :         7/        7 100.00% (100.00%),         0 (  0.00%) clones, pur 100.00%, hit eff  50.00%
31_noVelo+UT+T_fromLambda_P>5GeV_PT>500MeV        :         3/        3 100.00% (100.00%),         0 (  0.00%) clones, pur 100.00%, hit eff  50.00%
35_long_nSciFiHits_gt_0_AND_lt_5000               :      2624/     3225  81.36% ( 82.48%),         6 (  0.23%) clones, pur  99.86%, hit eff  49.86%


selreport_validator validation:
                                               Events  Candidates
Hlt1BGIPseudoPVsBeamOne:                            4           0
Hlt1BGIPseudoPVsBeamTwo:                            0           0
Hlt1BGIPseudoPVsDownBeamBeam:                       0           0
Hlt1BGIPseudoPVsIRBeamBeam:                         0           0
Hlt1BGIPseudoPVsNoBeam:                             0           0
Hlt1BGIPseudoPVsUpBeamBeam:                         0           0
Hlt1BeamGas:                                        0           0
Hlt1ConeJet100GeV:                                  0           0
Hlt1ConeJet15GeV:                                   0           0
Hlt1ConeJet30GeV:                                   0           0
Hlt1ConeJet50GeV:                                   0           0
Hlt1D2KK:                                           0           0
Hlt1D2KPi:                                          0           0
Hlt1D2KPiAlignment:                                 0           0
Hlt1D2Kshh:                                         0           0
Hlt1D2PiPi:                                         0           0
Hlt1DetJpsiToMuMuNegTagLine:                        0           0
Hlt1DetJpsiToMuMuPosTagLine:                        0           0
Hlt1DiElectronDisplaced:                            0           0
Hlt1DiElectronHighMass:                             0           0
Hlt1DiElectronHighMass_SS:                          0           0
Hlt1DiElectronLowMass_SS_massSlice1_displaced:      0           0
Hlt1DiElectronLowMass_SS_massSlice1_prompt:         0           0
Hlt1DiElectronLowMass_SS_massSlice2_displaced:      0           0
Hlt1DiElectronLowMass_SS_massSlice2_prompt:         0           0
Hlt1DiElectronLowMass_SS_massSlice3_displaced:      0           0
Hlt1DiElectronLowMass_SS_massSlice3_prompt:         0           0
Hlt1DiElectronLowMass_SS_massSlice4_displaced:      0           0
Hlt1DiElectronLowMass_SS_massSlice4_prompt:         0           0
Hlt1DiElectronLowMass_massSlice1_displaced:         0           0
Hlt1DiElectronLowMass_massSlice1_prompt:            0           0
Hlt1DiElectronLowMass_massSlice2_displaced:         0           0
Hlt1DiElectronLowMass_massSlice2_prompt:            0           0
Hlt1DiElectronLowMass_massSlice3_displaced:         0           0
Hlt1DiElectronLowMass_massSlice3_prompt:            0           0
Hlt1DiElectronLowMass_massSlice4_displaced:         0           0
Hlt1DiElectronLowMass_massSlice4_prompt:            0           0
Hlt1DiElectronSoft:                                 0           0
Hlt1DiMuonDisplaced:                                0           0
Hlt1DiMuonDrellYan:                                 0           0
Hlt1DiMuonDrellYan_SS:                              0           0
Hlt1DiMuonDrellYan_VLowMass:                        0           0
Hlt1DiMuonDrellYan_VLowMass_SS:                     0           0
Hlt1DiMuonHighMass:                                 0           0
Hlt1DiMuonJpsiMassAlignment:                        0           0
Hlt1DiMuonNoIP:                                     0           0
Hlt1DiMuonNoIP_SS:                                  0           0
Hlt1DiMuonSoft:                                     0           0
Hlt1DiPhotonHighMass:                               0           0
Hlt1Dst2D0Pi:                                       0           0
Hlt1Dst2D0PiAlignment:                              0           0
Hlt1ErrorBank:                                      0           0
Hlt1GECPassthrough:                                 0           0
Hlt1KsToPiPi:                                       0           0
Hlt1KsToPiPiDoubleMuonMisID:                        0           0
Hlt1LambdaLLDetachedTrack:                          0           0
Hlt1MaterialVertexSeedsDownstreamz:                 0           0
Hlt1MaterialVertexSeeds_DWFS:                       0           0
Hlt1ODIN1kHzLumi:                                   0           0
Hlt1ODINCalib:                                      0           0
Hlt1ODINLumi:                                       0           0
Hlt1ODINeeFarFromActivity:                          0           0
Hlt1OneMuonTrackLine:                               0           0
Hlt1Passthrough:                                    0           0
Hlt1PassthroughPVinSMOG2:                           0           0
Hlt1Pi02GammaGamma:                                 0           0
Hlt1RICH1Alignment:                                 0           0
Hlt1RICH2Alignment:                                 0           0
Hlt1SMOG22BodyGeneric:                              0           0
Hlt1SMOG22BodyGenericPrompt:                        1           1
Hlt1SMOG2BELowMultElectrons:                        2           0
Hlt1SMOG2BENoBias:                                  0           0
Hlt1SMOG2D2Kpi:                                     0           0
Hlt1SMOG2DiMuonHighMass:                            0           0
Hlt1SMOG2DisplacedDiMuon:                           0           0
Hlt1SMOG2JPsiToMuMuTaP_NegTag:                      0           0
Hlt1SMOG2JPsiToMuMuTaP_PosTag:                      0           0
Hlt1SMOG2KsTopipi:                                  1           1
Hlt1SMOG2L0Toppi:                                   1           1
Hlt1SMOG2MinimumBias:                               0           0
Hlt1SMOG2PassThroughLowMult5:                      10           0
Hlt1SMOG2SingleMuon:                                1           1
Hlt1SMOG2SingleTrackHighPt:                         0           0
Hlt1SMOG2SingleTrackVeryHighPt:                     0           0
Hlt1SMOG2etacTopp:                                  1           1
Hlt1SingleHighPtElectron:                           0           0
Hlt1SingleHighPtMuon:                               0           0
Hlt1SingleHighPtMuonNoMuID:                         0           0
Hlt1TAEPassthrough:                                 0           0
Hlt1TrackElectronMVA:                               0           0
Hlt1TrackMVA:                                       0           0
Hlt1TrackMuonMVA:                                   0           0
Hlt1TwoKs:                                          0           0
Hlt1TwoTrackKs:                                     0           0
Hlt1TwoTrackMVA:                                    0           0
Hlt1UpsilonAlignment:                               0           0
Hlt1VeloMicroBias:                                  0           0
Hlt1VeloMicroBiasVeloClosing:                       0           0
Hlt1XiOmegaLLL:                                     0           0

Total decisions:      21
Total tracks:         7
Total calos clusters: 0
Total SVs:            3
Total hits:           203
Total stdinfo:        89


velo_validator validation:
TrackChecker output                               :        49/     7513   0.65% ghosts
01_velo                                           :      4043/     4122  98.08% ( 98.83%),       195 (  4.60%) clones, pur  99.46%, hit eff  94.16%
02_long                                           :      2394/     2416  99.09% ( 99.52%),        97 (  3.89%) clones, pur  99.66%, hit eff  95.37%
03_long_P>5GeV                                    :      1589/     1602  99.19% ( 99.59%),        62 (  3.76%) clones, pur  99.82%, hit eff  96.04%
04_long_strange                                   :        99/      100  99.00% ( 98.48%),         5 (  4.81%) clones, pur  99.18%, hit eff  94.63%
05_long_strange_P>5GeV                            :        53/       54  98.15% ( 97.67%),         3 (  5.36%) clones, pur  99.91%, hit eff  95.91%
08_long_electrons                                 :       312/      315  99.05% ( 99.26%),        13 (  4.00%) clones, pur  98.22%, hit eff  94.84%

