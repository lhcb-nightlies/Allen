long_validator validation:
TrackChecker output                               :      2788/    43468   6.41% ghosts
for P>3GeV,Pt>0.5GeV                              :      1308/    27244   4.80% ghosts
01_long                                           :     38636/    78294  49.35% ( 51.30%),       339 (  0.87%) clones, pur  96.31%, hit eff  92.23%
02_long_P>5GeV                                    :     32379/    49257  65.73% ( 67.67%),       277 (  0.85%) clones, pur  96.61%, hit eff  92.89%
03_long_strange                                   :      1193/     3631  32.86% ( 32.95%),         8 (  0.67%) clones, pur  95.40%, hit eff  91.68%
04_long_strange_P>5GeV                            :       887/     1690  52.49% ( 54.34%),         2 (  0.22%) clones, pur  95.88%, hit eff  92.71%
05_long_fromB                                     :      3282/     4482  73.23% ( 74.57%),        27 (  0.82%) clones, pur  96.84%, hit eff  93.65%
06_long_fromB_P>5GeV                              :      3078/     3699  83.21% ( 83.52%),        27 (  0.87%) clones, pur  96.93%, hit eff  93.84%
07_long_electrons                                 :       761/     6100  12.48% ( 13.45%),        25 (  3.18%) clones, pur  95.34%, hit eff  92.41%
08_long_electrons_P>5GeV                          :       668/     3050  21.90% ( 23.43%),        21 (  3.05%) clones, pur  95.56%, hit eff  93.10%
09_long_fromB_electrons                           :        68/      218  31.19% ( 36.13%),         3 (  4.23%) clones, pur  95.87%, hit eff  94.01%
10_long_fromB_electrons_P>5GeV                    :        65/      141  46.10% ( 52.16%),         3 (  4.41%) clones, pur  95.99%, hit eff  94.44%
long_P>5GeV_AND_Pt>1GeV                           :      8355/     9736  85.82% ( 87.63%),        85 (  1.01%) clones, pur  96.77%, hit eff  93.39%
long_fromB_P>5GeV_AND_Pt>1GeV                     :      2061/     2313  89.11% ( 88.50%),        24 (  1.15%) clones, pur  97.06%, hit eff  94.18%
11_noVelo_UT                                      :         0/     8470   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
12_noVelo_UT_P>5GeV                               :         0/     3374   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
13_long_PT>2GeV                                   :      2075/     2348  88.37% ( 89.66%),        21 (  1.00%) clones, pur  96.87%, hit eff  93.86%
14_long_from_B_PT>2GeV                            :       901/     1008  89.38% ( 88.94%),        11 (  1.21%) clones, pur  96.90%, hit eff  94.05%
15_long_strange_P>5GeV                            :       887/     1690  52.49% ( 54.34%),         2 (  0.22%) clones, pur  95.88%, hit eff  92.71%
16_long_strange_P>5GeV_PT>500MeV                  :       464/      583  79.59% ( 80.36%),         1 (  0.22%) clones, pur  95.85%, hit eff  92.42%
17_long_fromSignal                                :      2171/     2608  83.24% ( 82.69%),        18 (  0.82%) clones, pur  96.93%, hit eff  93.92%


muon_validator validation:
Muon fraction in all MCPs:                                              15976/  1181264   0.01% 
Muon fraction in MCPs to which a track(s) was matched:                    459/    45320   0.01% 
Correctly identified muons with isMuon:                                   383/      459  83.44% 
Correctly identified muons from strange decays with isMuon:                 0/        0   -nan% 
Correctly identified muons from B decays with isMuon:                      80/       98  81.63% 
Tracks identified as muon with isMuon, but matched to non-muon MCP:      4359/    44861   9.72% 
Ghost tracks identified as muon with isMuon:                              312/     2788  11.19% 


pv_validator validation:
REC and MC vertices matched by dz distance
MC PV is reconstructible if at least 4 tracks are reconstructed
MC PV is isolated if dz to closest reconstructible MC PV > 10.00 mm
REC and MC vertices matched by dz distance

All                  :  0.928 (  5777/  6227)
Isolated             :  0.968 (  3010/  3110)
Close                :  0.888 (  2767/  3117)
False rate           :  0.013 (    75/  5852)
Real false rate      :  0.013 (    75/  5852)
Clones               :  0.000 (     0/  5777)


rate_validator validation:
Hlt1TrackMVA:                                     205/  1000, ( 6150.00 +/-   382.98) kHz
Hlt1TwoTrackMVA:                                  495/  1000, (14850.00 +/-   474.32) kHz
Hlt1D2KK:                                          11/  1000, (  330.00 +/-    98.95) kHz
Hlt1D2KPi:                                         17/  1000, (  510.00 +/-   122.64) kHz
Hlt1D2PiPi:                                        11/  1000, (  330.00 +/-    98.95) kHz
Hlt1Dst2D0Pi:                                       1/  1000, (   30.00 +/-    29.98) kHz
Hlt1KsToPiPi:                                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1KsToPiPiDoubleMuonMisID:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1TwoTrackKs:                                     7/  1000, (  210.00 +/-    79.09) kHz
Hlt1TwoKs:                                          0/  1000, (    0.00 +/-     0.00) kHz
Hlt1L02PPi:                                         5/  1000, (  150.00 +/-    66.91) kHz
Hlt1LambdaLLDetachedTrack:                          1/  1000, (   30.00 +/-    29.98) kHz
Hlt1XiOmegaLLL:                                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1OneMuonTrackLine:                               1/  1000, (   30.00 +/-    29.98) kHz
Hlt1SingleHighPtMuon:                               3/  1000, (   90.00 +/-    51.88) kHz
Hlt1SingleHighPtMuonNoMuID:                         2/  1000, (   60.00 +/-    42.38) kHz
Hlt1DiMuonHighMass:                                10/  1000, (  300.00 +/-    94.39) kHz
Hlt1DiMuonDisplaced:                                3/  1000, (   90.00 +/-    51.88) kHz
Hlt1DiMuonSoft:                                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1TrackMuonMVA:                                  14/  1000, (  420.00 +/-   111.46) kHz
Hlt1DiMuonNoIP:                                     4/  1000, (  120.00 +/-    59.88) kHz
Hlt1DiMuonNoIP_ss:                                  0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_VLowMass:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_VLowMass_SS:                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan:                                 1/  1000, (   30.00 +/-    29.98) kHz
Hlt1DiMuonDrellYan_SS:                              0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DetJpsiToMuMuPosTagLine:                        1/  1000, (   30.00 +/-    29.98) kHz
Hlt1DetJpsiToMuMuNegTagLine:                        2/  1000, (   60.00 +/-    42.38) kHz
Hlt1TrackElectronMVA:                              43/  1000, ( 1290.00 +/-   192.45) kHz
Hlt1SingleHighPtElectron:                           7/  1000, (  210.00 +/-    79.09) kHz
Hlt1DisplacedDielectron:                            6/  1000, (  180.00 +/-    73.26) kHz
Hlt1DiPhotonHighMass:                              24/  1000, (  720.00 +/-   145.20) kHz
Hlt1Pi02GammaGamma:                                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronHighMass_SameSign:                    0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiElectronHighMass:                             0/  1000, (    0.00 +/-     0.00) kHz
Hlt1LowMassDiElectron_massSlice1_prompt:            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1LowMassDiElectron_SS_massSlice1_prompt:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1LowMassDiElectron_massSlice2_prompt:            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1LowMassDiElectron_SS_massSlice2_prompt:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1LowMassDiElectron_massSlice3_prompt:            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1LowMassDiElectron_SS_massSlice3_prompt:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1LowMassDiElectron_massSlice4_prompt:            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1LowMassDiElectron_SS_massSlice4_prompt:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1LowMassDiElectron_massSlice1_displaced:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1LowMassDiElectron_SS_massSlice1_displaced:      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1LowMassDiElectron_massSlice2_displaced:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1LowMassDiElectron_SS_massSlice2_displaced:      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1LowMassDiElectron_massSlice3_displaced:         1/  1000, (   30.00 +/-    29.98) kHz
Hlt1LowMassDiElectron_SS_massSlice3_displaced:      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1LowMassDiElectron_massSlice4_displaced:         1/  1000, (   30.00 +/-    29.98) kHz
Hlt1LowMassDiElectron_SS_massSlice4_displaced:      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1Passthrough:                                    0/  1000, (    0.00 +/-     0.00) kHz
Hlt1TAEPassthrough:                                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsNoBeam:                             0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsBeamOne:                            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsBeamTwo:                            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsUpBeamBeam:                         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsDownBeamBeam:                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsIRBeamBeam:                        87/  1000, ( 2610.00 +/-   267.37) kHz
Hlt1ODINLumi:                                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ODIN1kHzLumi:                                   0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ODINNoBias:                                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ODINCalib:                                      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ErrorBank:                                      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1VeloMicroBiasVeloClosing:                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1RICH1Alignment:                                 1/  1000, (   30.00 +/-    29.98) kHz
Hlt1RICH2Alignment:                                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1D2KPiAlignment:                                 5/  1000, (  150.00 +/-    66.91) kHz
Hlt1MaterialVertexSeedsDownstreamz:                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1MaterialVertexSeeds_DWFS:                       1/  1000, (   30.00 +/-    29.98) kHz
Hlt1Dst2D0PiAlignment:                              1/  1000, (   30.00 +/-    29.98) kHz
Hlt1DiMuonHighMassAlignment:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonJpsiMassAlignment:                       15/  1000, (  450.00 +/-   115.31) kHz
Hlt1DisplacedDiMuonAlignment:                       1/  1000, (   30.00 +/-    29.98) kHz
Hlt1BeamGas:                                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1VeloMicroBias:                                  0/  1000, (    0.00 +/-     0.00) kHz
Hlt1_BESMOG2_NoBias:                                0/  1000, (    0.00 +/-     0.00) kHz
Hlt1GECPassThrough_LowMult5:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1_BESMOG2_LowMult10:                             0/  1000, (    0.00 +/-     0.00) kHz
Hlt1_SMOG2_MinimumBias:                             0/  1000, (    0.00 +/-     0.00) kHz
Hlt1Passthrough_PV_in_SMOG2:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1_SMOG2_D2Kpi:                                   0/  1000, (    0.00 +/-     0.00) kHz
Hlt1_SMOG2_eta2pp:                                  0/  1000, (    0.00 +/-     0.00) kHz
Hlt1_SMOG2_KsToPiPi:                                0/  1000, (    0.00 +/-     0.00) kHz
Hlt1_SMOG2_2BodyGeneric:                            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1_SMOG2_SingleTrack:                             0/  1000, (    0.00 +/-     0.00) kHz
Hlt1_SMOG2_DiMuonHighMass:                          0/  1000, (    0.00 +/-     0.00) kHz
Hlt1_SMOG2_SingleMuon:                              0/  1000, (    0.00 +/-     0.00) kHz
Hlt1_SMOG2_L02PPi:                                  0/  1000, (    0.00 +/-     0.00) kHz
Inclusive:                                        586/  1000, (17580.00 +/-   467.27) kHz


selreport_validator validation:
                                               Events  Candidates
Hlt1TrackMVA:                                     205         311
Hlt1TwoTrackMVA:                                  495        1416
Hlt1D2KK:                                          11          12
Hlt1D2KPi:                                         17          18
Hlt1D2PiPi:                                        11          11
Hlt1Dst2D0Pi:                                       1           1
Hlt1KsToPiPi:                                       0           0
Hlt1KsToPiPiDoubleMuonMisID:                        0           0
Hlt1TwoTrackKs:                                     7           7
Hlt1TwoKs:                                          0           0
Hlt1L02PPi:                                         5           5
Hlt1LambdaLLDetachedTrack:                          1           1
Hlt1XiOmegaLLL:                                     0           0
Hlt1OneMuonTrackLine:                               1           0
Hlt1SingleHighPtMuon:                               3           3
Hlt1SingleHighPtMuonNoMuID:                         2           3
Hlt1DiMuonHighMass:                                10          11
Hlt1DiMuonDisplaced:                                3           3
Hlt1DiMuonSoft:                                     0           0
Hlt1TrackMuonMVA:                                  14          14
Hlt1DiMuonNoIP:                                     4           4
Hlt1DiMuonNoIP_ss:                                  0           0
Hlt1DiMuonDrellYan_VLowMass:                        0           0
Hlt1DiMuonDrellYan_VLowMass_SS:                     0           0
Hlt1DiMuonDrellYan:                                 1           1
Hlt1DiMuonDrellYan_SS:                              0           0
Hlt1DetJpsiToMuMuPosTagLine:                        1           1
Hlt1DetJpsiToMuMuNegTagLine:                        2           2
Hlt1TrackElectronMVA:                              43          44
Hlt1SingleHighPtElectron:                           7           7
Hlt1DisplacedDielectron:                            6           7
Hlt1DiPhotonHighMass:                              24          31
Hlt1Pi02GammaGamma:                                 0           0
Hlt1DiElectronHighMass_SameSign:                    0           0
Hlt1DiElectronHighMass:                             0           0
Hlt1LowMassDiElectron_massSlice1_prompt:            0           0
Hlt1LowMassDiElectron_SS_massSlice1_prompt:         0           0
Hlt1LowMassDiElectron_massSlice2_prompt:            0           0
Hlt1LowMassDiElectron_SS_massSlice2_prompt:         0           0
Hlt1LowMassDiElectron_massSlice3_prompt:            0           0
Hlt1LowMassDiElectron_SS_massSlice3_prompt:         0           0
Hlt1LowMassDiElectron_massSlice4_prompt:            0           0
Hlt1LowMassDiElectron_SS_massSlice4_prompt:         0           0
Hlt1LowMassDiElectron_massSlice1_displaced:         0           0
Hlt1LowMassDiElectron_SS_massSlice1_displaced:      0           0
Hlt1LowMassDiElectron_massSlice2_displaced:         0           0
Hlt1LowMassDiElectron_SS_massSlice2_displaced:      0           0
Hlt1LowMassDiElectron_massSlice3_displaced:         1           1
Hlt1LowMassDiElectron_SS_massSlice3_displaced:      0           0
Hlt1LowMassDiElectron_massSlice4_displaced:         1           1
Hlt1LowMassDiElectron_SS_massSlice4_displaced:      0           0
Hlt1Passthrough:                                    0           0
Hlt1TAEPassthrough:                                 0           0
Hlt1BGIPseudoPVsNoBeam:                             0           0
Hlt1BGIPseudoPVsBeamOne:                            0           0
Hlt1BGIPseudoPVsBeamTwo:                            0           0
Hlt1BGIPseudoPVsUpBeamBeam:                         0           0
Hlt1BGIPseudoPVsDownBeamBeam:                       0           0
Hlt1BGIPseudoPVsIRBeamBeam:                        87           0
Hlt1ODINLumi:                                       0           0
Hlt1ODIN1kHzLumi:                                   0           0
Hlt1ODINNoBias:                                     0           0
Hlt1ODINCalib:                                      0           0
Hlt1ErrorBank:                                      0           0
Hlt1VeloMicroBiasVeloClosing:                       0           0
Hlt1RICH1Alignment:                                 1           2
Hlt1RICH2Alignment:                                 0           0
Hlt1D2KPiAlignment:                                 5           5
Hlt1MaterialVertexSeedsDownstreamz:                 0           0
Hlt1MaterialVertexSeeds_DWFS:                       1           0
Hlt1Dst2D0PiAlignment:                              1           1
Hlt1DiMuonHighMassAlignment:                        0           0
Hlt1DiMuonJpsiMassAlignment:                       15          16
Hlt1DisplacedDiMuonAlignment:                       1           1
Hlt1BeamGas:                                        0           0
Hlt1VeloMicroBias:                                  0           0
Hlt1_BESMOG2_NoBias:                                0           0
Hlt1GECPassThrough_LowMult5:                        0           0
Hlt1_BESMOG2_LowMult10:                             0           0
Hlt1_SMOG2_MinimumBias:                             0           0
Hlt1Passthrough_PV_in_SMOG2:                        0           0
Hlt1_SMOG2_D2Kpi:                                   0           0
Hlt1_SMOG2_eta2pp:                                  0           0
Hlt1_SMOG2_KsToPiPi:                                0           0
Hlt1_SMOG2_2BodyGeneric:                            0           0
Hlt1_SMOG2_SingleTrack:                             0           0
Hlt1_SMOG2_DiMuonHighMass:                          0           0
Hlt1_SMOG2_SingleMuon:                              0           0
Hlt1_SMOG2_L02PPi:                                  0           0

Total decisions:      987
Total tracks:         1759
Total calos clusters: 54
Total SVs:            1498
Total hits:           45611
Total stdinfo:        21267


veloUT_validator validation:
TrackChecker output                               :      5379/    69701   7.72% ghosts
01_velo                                           :     60933/   138014  44.15% ( 44.69%),       529 (  0.86%) clones, pur  99.42%, hit eff  96.69%
02_velo+UT                                        :     60803/   120107  50.62% ( 51.20%),       528 (  0.86%) clones, pur  99.44%, hit eff  96.69%
03_velo+UT_P>5GeV                                 :     42569/    59409  71.65% ( 72.44%),       353 (  0.82%) clones, pur  99.55%, hit eff  97.59%
04_velo+notLong                                   :     12909/    59720  21.62% ( 21.79%),       102 (  0.78%) clones, pur  99.16%, hit eff  95.49%
05_velo+UT+notLong                                :     12795/    42823  29.88% ( 30.12%),       102 (  0.79%) clones, pur  99.24%, hit eff  95.47%
06_velo+UT+notLong_P>5GeV                         :      6683/    10995  60.78% ( 61.43%),        44 (  0.65%) clones, pur  99.48%, hit eff  97.62%
07_long                                           :     48024/    78294  61.34% ( 62.08%),       427 (  0.88%) clones, pur  99.49%, hit eff  97.01%
08_long_P>5GeV                                    :     35902/    49257  72.89% ( 73.67%),       310 (  0.86%) clones, pur  99.56%, hit eff  97.58%
09_long_fromB                                     :      3716/     4482  82.91% ( 84.81%),        31 (  0.83%) clones, pur  99.48%, hit eff  97.42%
10_long_fromB_P>5GeV                              :      3313/     3699  89.56% ( 90.58%),        29 (  0.87%) clones, pur  99.51%, hit eff  97.57%
11_long_electrons                                 :      1073/     6100  17.59% ( 18.39%),        32 (  2.90%) clones, pur  97.73%, hit eff  94.87%
12_long_fromB_electrons                           :        90/      218  41.28% ( 46.33%),         4 (  4.26%) clones, pur  98.20%, hit eff  95.90%
13_long_fromB_electrons_P>5GeV                    :        81/      141  57.45% ( 64.22%),         4 (  4.71%) clones, pur  98.38%, hit eff  97.35%


velo_validator validation:
TrackChecker output                               :      3030/   319876   0.95% ghosts
01_velo                                           :    135674/   138014  98.30% ( 98.38%),      2965 (  2.14%) clones, pur  99.66%, hit eff  96.63%
02_long                                           :     77802/    78294  99.37% ( 99.42%),      1231 (  1.56%) clones, pur  99.77%, hit eff  97.73%
03_long_P>5GeV                                    :     49085/    49257  99.65% ( 99.65%),       645 (  1.30%) clones, pur  99.80%, hit eff  98.28%
04_long_strange                                   :      3556/     3631  97.93% ( 98.41%),        54 (  1.50%) clones, pur  99.34%, hit eff  97.27%
05_long_strange_P>5GeV                            :      1662/     1690  98.34% ( 98.55%),        10 (  0.60%) clones, pur  99.27%, hit eff  98.59%
06_long_fromB                                     :      4447/     4482  99.22% ( 99.40%),        59 (  1.31%) clones, pur  99.68%, hit eff  97.82%
07_long_fromB_P>5GeV                              :      3682/     3699  99.54% ( 99.57%),        42 (  1.13%) clones, pur  99.71%, hit eff  98.19%
08_long_electrons                                 :      5925/     6100  97.13% ( 97.03%),       184 (  3.01%) clones, pur  97.88%, hit eff  96.60%
09_long_fromB_electrons                           :       210/      218  96.33% ( 96.57%),        11 (  4.98%) clones, pur  98.07%, hit eff  96.67%
10_long_fromB_electrons_P>5GeV                    :       137/      141  97.16% ( 97.70%),         9 (  6.16%) clones, pur  98.57%, hit eff  97.13%
11_long_fromSignal                                :      2580/     2608  98.93% ( 99.28%),        25 (  0.96%) clones, pur  99.67%, hit eff  98.09%

