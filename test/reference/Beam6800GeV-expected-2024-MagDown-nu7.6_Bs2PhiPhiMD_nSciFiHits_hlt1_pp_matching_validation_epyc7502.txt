long_validator validation:
TrackChecker output                               :      1355/    29980   4.52% ghosts
for P>3GeV,Pt>0.5GeV                              :       290/    15841   1.83% ghosts
01_long                                           :     26540/    37926  69.98% ( 70.59%),       554 (  2.04%) clones, pur  98.74%, hit eff  98.21%
02_long_P>5GeV                                    :     21462/    24714  86.84% ( 87.59%),       463 (  2.11%) clones, pur  98.82%, hit eff  98.51%
03_long_strange                                   :      1017/     1807  56.28% ( 56.50%),        17 (  1.64%) clones, pur  98.18%, hit eff  97.92%
04_long_strange_P>5GeV                            :       707/      878  80.52% ( 80.15%),        12 (  1.67%) clones, pur  98.29%, hit eff  98.25%
05_long_fromB                                     :      1930/     2309  83.59% ( 84.21%),        41 (  2.08%) clones, pur  99.05%, hit eff  98.57%
06_long_fromB_P>5GeV                              :      1774/     1909  92.93% ( 93.07%),        39 (  2.15%) clones, pur  99.11%, hit eff  98.70%
07_long_electrons                                 :       908/     2697  33.67% ( 33.46%),        32 (  3.40%) clones, pur  96.31%, hit eff  98.20%
08_long_electrons_P>5GeV                          :       712/     1402  50.78% ( 51.49%),        29 (  3.91%) clones, pur  96.53%, hit eff  98.42%
09_long_fromB_electrons                           :        52/      110  47.27% ( 49.38%),         0 (  0.00%) clones, pur  97.96%, hit eff  98.77%
10_long_fromB_electrons_P>5GeV                    :        47/       74  63.51% ( 65.62%),         0 (  0.00%) clones, pur  98.52%, hit eff  98.81%
long_P>5GeV_AND_Pt>1GeV                           :      5896/     6486  90.90% ( 91.65%),       139 (  2.30%) clones, pur  98.98%, hit eff  98.57%
long_fromB_P>5GeV_AND_Pt>1GeV                     :      1202/     1281  93.83% ( 93.93%),        26 (  2.12%) clones, pur  99.07%, hit eff  98.73%
11_noVelo_UT                                      :         0/     4124   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
12_noVelo_UT_P>5GeV                               :         0/     1671   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
13_long_PT>2GeV                                   :      1491/     1586  94.01% ( 93.76%),        23 (  1.52%) clones, pur  98.97%, hit eff  98.89%
14_long_from_B_PT>2GeV                            :       585/      605  96.69% ( 96.52%),         5 (  0.85%) clones, pur  99.05%, hit eff  98.94%
15_long_strange_P>5GeV                            :       707/      878  80.52% ( 80.15%),        12 (  1.67%) clones, pur  98.29%, hit eff  98.25%
16_long_strange_P>5GeV_PT>500MeV                  :       294/      335  87.76% ( 87.57%),         5 (  1.67%) clones, pur  98.85%, hit eff  98.31%
17_long_fromSignal                                :      1183/     1312  90.17% ( 89.96%),        26 (  2.15%) clones, pur  99.11%, hit eff  98.62%
18_long_nSciFiHits_gt_0_AND_lt_5000               :     11573/    16638  69.56% ( 69.41%),       257 (  2.17%) clones, pur  98.94%, hit eff  98.67%
19_long_nSciFiHits_gt_5000_AND_lt_7000            :     10115/    14945  67.68% ( 67.92%),       195 (  1.89%) clones, pur  98.58%, hit eff  98.09%
20_long_nSciFiHits_gt_7000_AND_lt_10000           :      5517/     8507  64.85% ( 65.08%),       127 (  2.25%) clones, pur  98.23%, hit eff  97.51%
21_long_nSciFiHits_gt_10000                       :       146/      409  35.70% ( 35.77%),         4 (  2.67%) clones, pur  97.20%, hit eff  97.09%


muon_validator validation:
Muon fraction in all MCPs:                                               7257/   574938   0.01% 
Muon fraction in MCPs to which a track(s) was matched:                    274/    32523   0.01% 
Correctly identified muons with isMuon:                                   241/      274  87.96% 
Correctly identified muons from strange decays with isMuon:                 1/        1 100.00% 
Correctly identified muons from B decays with isMuon:                      56/       60  93.33% 
Tracks identified as muon with isMuon, but matched to non-muon MCP:      3648/    32249  11.31% 
Ghost tracks identified as muon with isMuon:                              228/     1355  16.83% 


pv_validator validation:
REC and MC vertices matched by dz distance
MC PV is reconstructible if at least 4 tracks are reconstructed
MC PV is isolated if dz to closest reconstructible MC PV > 10.00 mm
REC and MC vertices matched by dz distance

All                  :  0.942 (  2764/  2933)
Isolated             :  0.977 (  1480/  1515)
Close                :  0.906 (  1284/  1418)
False rate           :  0.020 (    55/  2819)
Real false rate      :  0.020 (    55/  2819)
Clones               :  0.000 (     0/  2764)


rate_validator validation:
Hlt1BGIPseudoPVsBeamOne:                            0/   500, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsBeamTwo:                            0/   500, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsDownBeamBeam:                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsIRBeamBeam:                         1/   500, (   60.00 +/-    59.94) kHz
Hlt1BGIPseudoPVsNoBeam:                             0/   500, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsUpBeamBeam:                         0/   500, (    0.00 +/-     0.00) kHz
Hlt1BeamGas:                                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1ConeJet100GeV:                                  0/   500, (    0.00 +/-     0.00) kHz
Hlt1ConeJet15GeV:                                   0/   500, (    0.00 +/-     0.00) kHz
Hlt1ConeJet30GeV:                                   0/   500, (    0.00 +/-     0.00) kHz
Hlt1ConeJet50GeV:                                   0/   500, (    0.00 +/-     0.00) kHz
Hlt1D2KK:                                          15/   500, (  900.00 +/-   228.87) kHz
Hlt1D2KPi:                                         22/   500, ( 1320.00 +/-   275.16) kHz
Hlt1D2KPiAlignment:                                 8/   500, (  480.00 +/-   168.34) kHz
Hlt1D2Kshh:                                         3/   500, (  180.00 +/-   103.61) kHz
Hlt1D2PiPi:                                        11/   500, (  660.00 +/-   196.80) kHz
Hlt1DetJpsiToMuMuNegTagLine:                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1DetJpsiToMuMuPosTagLine:                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronDisplaced:                            9/   500, (  540.00 +/-   178.37) kHz
Hlt1DiElectronHighMass:                             1/   500, (   60.00 +/-    59.94) kHz
Hlt1DiElectronHighMass_SS:                          1/   500, (   60.00 +/-    59.94) kHz
Hlt1DiElectronLowMass_SS_massSlice1_displaced:      0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice1_prompt:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice2_displaced:      0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice2_prompt:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice3_displaced:      0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice3_prompt:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice4_displaced:      0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice4_prompt:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice1_displaced:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice1_prompt:            0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice2_displaced:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice2_prompt:            0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice3_displaced:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice3_prompt:            0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice4_displaced:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice4_prompt:            0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronSoft:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDisplaced:                                9/   500, (  540.00 +/-   178.37) kHz
Hlt1DiMuonDrellYan:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_SS:                              0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_VLowMass:                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_VLowMass_SS:                     0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonHighMass:                                 8/   500, (  480.00 +/-   168.34) kHz
Hlt1DiMuonJpsiMassAlignment:                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonNoIP:                                     0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonNoIP_SS:                                  0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonSoft:                                     0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiPhotonHighMass:                               3/   500, (  180.00 +/-   103.61) kHz
Hlt1Dst2D0Pi:                                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1Dst2D0PiAlignment:                              0/   500, (    0.00 +/-     0.00) kHz
Hlt1ErrorBank:                                      0/   500, (    0.00 +/-     0.00) kHz
Hlt1GECPassthrough:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1KsToPiPi:                                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1KsToPiPiDoubleMuonMisID:                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1LambdaLLDetachedTrack:                          2/   500, (  120.00 +/-    84.68) kHz
Hlt1MaterialVertexSeedsDownstreamz:                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1MaterialVertexSeeds_DWFS:                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1ODIN1kHzLumi:                                   0/   500, (    0.00 +/-     0.00) kHz
Hlt1ODINCalib:                                      0/   500, (    0.00 +/-     0.00) kHz
Hlt1ODINLumi:                                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1ODINeeFarFromActivity:                          0/   500, (    0.00 +/-     0.00) kHz
Hlt1OneMuonTrackLine:                               1/   500, (   60.00 +/-    59.94) kHz
Hlt1Passthrough:                                    0/   500, (    0.00 +/-     0.00) kHz
Hlt1PassthroughPVinSMOG2:                           0/   500, (    0.00 +/-     0.00) kHz
Hlt1Pi02GammaGamma:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1RICH1Alignment:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1RICH2Alignment:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG22BodyGeneric:                              0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG22BodyGenericPrompt:                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2BELowMultElectrons:                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2BENoBias:                                  0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2D2Kpi:                                     0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2DiMuonHighMass:                            0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2DisplacedDiMuon:                           0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2JPsiToMuMuTaP_NegTag:                      0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2JPsiToMuMuTaP_PosTag:                      0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2KsTopipi:                                  0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2L0Toppi:                                   0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2MinimumBias:                               0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2PassThroughLowMult5:                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2SingleMuon:                                0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2SingleTrackHighPt:                         0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2SingleTrackVeryHighPt:                     0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2etacTopp:                                  0/   500, (    0.00 +/-     0.00) kHz
Hlt1SingleHighPtElectron:                           5/   500, (  300.00 +/-   133.49) kHz
Hlt1SingleHighPtMuon:                               1/   500, (   60.00 +/-    59.94) kHz
Hlt1SingleHighPtMuonNoMuID:                         4/   500, (  240.00 +/-   119.52) kHz
Hlt1TAEPassthrough:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1TrackElectronMVA:                              23/   500, ( 1380.00 +/-   281.05) kHz
Hlt1TrackMVA:                                     121/   500, ( 7260.00 +/-   574.62) kHz
Hlt1TrackMuonMVA:                                   9/   500, (  540.00 +/-   178.37) kHz
Hlt1TwoKs:                                          0/   500, (    0.00 +/-     0.00) kHz
Hlt1TwoTrackKs:                                     2/   500, (  120.00 +/-    84.68) kHz
Hlt1TwoTrackMVA:                                  286/   500, (17160.00 +/-   663.83) kHz
Hlt1UpsilonAlignment:                               0/   500, (    0.00 +/-     0.00) kHz
Hlt1VeloMicroBias:                                  1/   500, (   60.00 +/-    59.94) kHz
Hlt1VeloMicroBiasVeloClosing:                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1XiOmegaLLL:                                     0/   500, (    0.00 +/-     0.00) kHz
Inclusive:                                        310/   500, (18600.00 +/-   651.21) kHz


seed_validator validation:
TrackChecker output                               :      4446/    52279   8.50% ghosts
for P>3GeV,Pt>0.5GeV                              :         0/        0   -nan% ghosts
00_P>3Gev_Pt>0.5                                  :     15457/    17038  90.72% ( 91.47%),       230 (  1.47%) clones, pur  99.32%, hit eff  97.71%
01_long                                           :     29448/    37926  77.65% ( 78.12%),       405 (  1.36%) clones, pur  99.38%, hit eff  97.74%
---1. phi quadrant                                :      7319/     9440  77.53% ( 77.86%),       124 (  1.67%) clones, pur  99.30%, hit eff  97.70%
---2. phi quadrant                                :      7484/     9611  77.87% ( 78.65%),        95 (  1.25%) clones, pur  99.39%, hit eff  97.70%
---3. phi quadrant                                :      7288/     9375  77.74% ( 77.86%),        89 (  1.21%) clones, pur  99.40%, hit eff  97.78%
---4. phi quadrant                                :      7356/     9499  77.44% ( 77.56%),        97 (  1.30%) clones, pur  99.41%, hit eff  97.78%
---eta < 2.5, small x, large y                    :       509/     1614  31.54% ( 30.45%),         4 (  0.78%) clones, pur  98.87%, hit eff  94.97%
---eta < 2.5, large x, small y                    :      1473/     2835  51.96% ( 51.56%),        26 (  1.73%) clones, pur  99.15%, hit eff  96.66%
---eta > 2.5, small x, large y                    :     10047/    12171  82.55% ( 83.11%),       135 (  1.33%) clones, pur  99.37%, hit eff  97.73%
---eta > 2.5, large x, small y                    :     17419/    21306  81.76% ( 81.97%),       240 (  1.36%) clones, pur  99.41%, hit eff  97.92%
02_long_P>5GeV                                    :     23304/    24714  94.29% ( 95.02%),       347 (  1.47%) clones, pur  99.37%, hit eff  98.02%
02_long_P>5GeV, eta > 4                           :      9337/     9981  93.55% ( 94.44%),       147 (  1.55%) clones, pur  99.27%, hit eff  97.80%
---eta < 2.5, small x, large y                    :       394/      479  82.25% ( 82.00%),         3 (  0.76%) clones, pur  99.17%, hit eff  97.05%
---eta < 2.5, large x, small y                    :       907/      952  95.27% ( 95.66%),        15 (  1.63%) clones, pur  99.59%, hit eff  98.37%
---eta > 2.5, small x, large y                    :      8228/     8612  95.54% ( 96.22%),       120 (  1.44%) clones, pur  99.35%, hit eff  98.04%
---eta > 2.5, large x, small y                    :     13775/    14671  93.89% ( 94.56%),       209 (  1.49%) clones, pur  99.38%, hit eff  98.01%
03_long_P>3GeV                                    :     29443/    32393  90.89% ( 91.52%),       405 (  1.36%) clones, pur  99.38%, hit eff  97.74%
04_long_P>0.5GeV                                  :     29448/    37926  77.65% ( 78.12%),       405 (  1.36%) clones, pur  99.38%, hit eff  97.74%
05_long_from_B                                    :      2013/     2309  87.18% ( 87.86%),        23 (  1.13%) clones, pur  99.54%, hit eff  98.24%
06_long_from_B_P>5GeV                             :      1830/     1909  95.86% ( 95.94%),        21 (  1.13%) clones, pur  99.58%, hit eff  98.43%
07_long_from_B_P>3GeV                             :      2013/     2155  93.41% ( 93.15%),        23 (  1.13%) clones, pur  99.54%, hit eff  98.24%
08_UT+SciFi                                       :      3135/     5429  57.75% ( 56.79%),        32 (  1.01%) clones, pur  99.30%, hit eff  96.88%
09_UT+SciFi_P>5GeV                                :      2044/     2268  90.12% ( 91.03%),        24 (  1.16%) clones, pur  99.31%, hit eff  97.32%
10_UT+SciFi_P>3GeV                                :      3123/     3817  81.82% ( 82.18%),        32 (  1.01%) clones, pur  99.31%, hit eff  96.91%
11_UT+SciFi_fromStrange                           :      1433/     1984  72.23% ( 72.31%),        15 (  1.04%) clones, pur  99.52%, hit eff  97.64%
12_UT+SciFi_fromStrange_P>5GeV                    :      1049/     1114  94.17% ( 95.09%),        13 (  1.22%) clones, pur  99.52%, hit eff  97.95%
13_UT+SciFi_fromStrange_P>3GeV                    :      1432/     1613  88.78% ( 89.48%),        15 (  1.04%) clones, pur  99.52%, hit eff  97.65%
14_long_electrons                                 :      1446/     2697  53.62% ( 52.44%),        19 (  1.30%) clones, pur  99.42%, hit eff  97.64%
15_long_electrons_P>5GeV                          :      1077/     1402  76.82% ( 76.04%),        16 (  1.46%) clones, pur  99.51%, hit eff  97.90%
16_long_electrons_P>3GeV                          :      1446/     2178  66.39% ( 65.84%),        19 (  1.30%) clones, pur  99.42%, hit eff  97.64%
17_long_fromB_electrons                           :        72/      110  65.45% ( 67.49%),         1 (  1.37%) clones, pur  99.24%, hit eff  97.86%
18_long_fromB_electrons_P>5GeV                    :        62/       74  83.78% ( 85.16%),         1 (  1.59%) clones, pur  99.68%, hit eff  98.18%
19_long_PT>2GeV                                   :      1525/     1586  96.15% ( 96.31%),        17 (  1.10%) clones, pur  99.54%, hit eff  98.53%
20_long_from_B_PT>2GeV                            :       595/      605  98.35% ( 97.88%),         3 (  0.50%) clones, pur  99.79%, hit eff  98.83%
21_long_strange_P>5GeV                            :       827/      878  94.19% ( 94.90%),        12 (  1.43%) clones, pur  99.35%, hit eff  97.70%
22_long_strange_P>5GeV_PT>500MeV                  :       321/      335  95.82% ( 96.05%),         5 (  1.53%) clones, pur  99.57%, hit eff  97.78%
23_noVelo+UT+T_fromSignal                         :        93/      168  55.36% ( 56.67%),         2 (  2.11%) clones, pur  98.52%, hit eff  96.01%
24_noVelo+UT+T_fromKs0                            :       909/     1242  73.19% ( 72.29%),        10 (  1.09%) clones, pur  99.55%, hit eff  97.56%
25_noVelo+UT+T_fromLambda                         :       499/      692  72.11% ( 72.74%),         5 (  0.99%) clones, pur  99.45%, hit eff  97.80%
26_noVelo+UT+T_fromSignal_P>5GeV                  :        62/       73  84.93% ( 84.83%),         1 (  1.59%) clones, pur  98.36%, hit eff  96.65%
27_noVelo+UT+T_fromKs0_P>5GeV                     :       651/      685  95.04% ( 95.01%),         8 (  1.21%) clones, pur  99.58%, hit eff  98.08%
28_noVelo+UT+T_fromLambda_P>5GeV                  :       386/      416  92.79% ( 93.35%),         5 (  1.28%) clones, pur  99.36%, hit eff  97.80%
29_noVelo+UT+T_fromSignal_P>5GeV_PT>500MeV        :        43/       52  82.69% ( 81.67%),         1 (  2.27%) clones, pur  97.66%, hit eff  95.39%
30_noVelo+UT+T_fromKs0_P>5GeV_PT>500MeV           :       360/      383  93.99% ( 94.22%),         3 (  0.83%) clones, pur  99.62%, hit eff  98.34%
31_noVelo+UT+T_fromLambda_P>5GeV_PT>500MeV        :       261/      281  92.88% ( 93.08%),         4 (  1.51%) clones, pur  99.51%, hit eff  97.89%
35_long_nSciFiHits_gt_0_AND_lt_5000               :     13621/    17922  76.00% ( 75.82%),       142 (  1.03%) clones, pur  99.58%, hit eff  98.29%
36_long_nSciFiHits_gt_5000_AND_lt_7000            :     12042/    16105  74.77% ( 74.92%),       179 (  1.46%) clones, pur  99.28%, hit eff  97.50%
37_long_nSciFiHits_gt_7000_AND_lt_10000           :      6651/     9149  72.70% ( 72.85%),       127 (  1.87%) clones, pur  99.05%, hit eff  96.85%
38_long_nSciFiHits_gt_10000                       :       177/      449  39.42% ( 39.49%),         2 (  1.12%) clones, pur  98.98%, hit eff  96.19%


seed_xz_validator validation:
TrackChecker output                               :     21962/    75681  29.02% ghosts
for P>3GeV,Pt>0.5GeV                              :         0/        0   -nan% ghosts
00_P>3Gev_Pt>0.5                                  :     15950/    17038  93.61% ( 94.30%),       170 (  1.05%) clones, pur  99.41%, hit eff  49.06%
01_long                                           :     30359/    37926  80.05% ( 80.51%),       331 (  1.08%) clones, pur  99.42%, hit eff  49.01%
---1. phi quadrant                                :      7560/     9440  80.08% ( 80.39%),        83 (  1.09%) clones, pur  99.41%, hit eff  49.04%
---2. phi quadrant                                :      7695/     9611  80.06% ( 80.83%),        93 (  1.19%) clones, pur  99.38%, hit eff  48.95%
---3. phi quadrant                                :      7522/     9375  80.23% ( 80.33%),        77 (  1.01%) clones, pur  99.41%, hit eff  49.01%
---4. phi quadrant                                :      7581/     9499  79.81% ( 80.00%),        78 (  1.02%) clones, pur  99.47%, hit eff  49.06%
---eta < 2.5, small x, large y                    :       589/     1614  36.49% ( 35.40%),         5 (  0.84%) clones, pur  98.13%, hit eff  48.11%
---eta < 2.5, large x, small y                    :      1565/     2835  55.20% ( 54.51%),        23 (  1.45%) clones, pur  99.39%, hit eff  48.91%
---eta > 2.5, small x, large y                    :     10250/    12171  84.22% ( 84.71%),       107 (  1.03%) clones, pur  99.43%, hit eff  48.99%
---eta > 2.5, large x, small y                    :     17955/    21306  84.27% ( 84.61%),       196 (  1.08%) clones, pur  99.46%, hit eff  49.07%
02_long_P>5GeV                                    :     23844/    24714  96.48% ( 97.17%),       276 (  1.14%) clones, pur  99.43%, hit eff  49.09%
02_long_P>5GeV, eta > 4                           :      9607/     9981  96.25% ( 97.12%),       126 (  1.29%) clones, pur  99.33%, hit eff  49.04%
---eta < 2.5, small x, large y                    :       410/      479  85.59% ( 84.92%),         4 (  0.97%) clones, pur  98.95%, hit eff  48.94%
---eta < 2.5, large x, small y                    :       924/      952  97.06% ( 97.18%),        12 (  1.28%) clones, pur  99.63%, hit eff  49.27%
---eta > 2.5, small x, large y                    :      8325/     8612  96.67% ( 97.29%),        88 (  1.05%) clones, pur  99.42%, hit eff  49.03%
---eta > 2.5, large x, small y                    :     14185/    14671  96.69% ( 97.40%),       172 (  1.20%) clones, pur  99.43%, hit eff  49.12%
03_long_P>3GeV                                    :     30353/    32393  93.70% ( 94.31%),       331 (  1.08%) clones, pur  99.42%, hit eff  49.01%
04_long_P>0.5GeV                                  :     30359/    37926  80.05% ( 80.51%),       331 (  1.08%) clones, pur  99.42%, hit eff  49.01%
05_long_from_B                                    :      2061/     2309  89.26% ( 89.57%),        17 (  0.82%) clones, pur  99.60%, hit eff  49.30%
06_long_from_B_P>5GeV                             :      1862/     1909  97.54% ( 97.25%),        15 (  0.80%) clones, pur  99.63%, hit eff  49.40%
07_long_from_B_P>3GeV                             :      2061/     2155  95.64% ( 95.07%),        17 (  0.82%) clones, pur  99.60%, hit eff  49.30%
08_UT+SciFi                                       :      3492/     5429  64.32% ( 63.18%),        37 (  1.05%) clones, pur  99.32%, hit eff  48.89%
09_UT+SciFi_P>5GeV                                :      2196/     2268  96.83% ( 97.49%),        25 (  1.13%) clones, pur  99.36%, hit eff  49.01%
10_UT+SciFi_P>3GeV                                :      3471/     3817  90.94% ( 91.40%),        37 (  1.05%) clones, pur  99.33%, hit eff  48.90%
11_UT+SciFi_fromStrange                           :      1506/     1984  75.91% ( 75.87%),        17 (  1.12%) clones, pur  99.38%, hit eff  48.93%
12_UT+SciFi_fromStrange_P>5GeV                    :      1086/     1114  97.49% ( 98.38%),        14 (  1.27%) clones, pur  99.34%, hit eff  48.94%
13_UT+SciFi_fromStrange_P>3GeV                    :      1503/     1613  93.18% ( 93.59%),        17 (  1.12%) clones, pur  99.39%, hit eff  48.94%
14_long_electrons                                 :      1487/     2697  55.14% ( 53.97%),        19 (  1.26%) clones, pur  99.22%, hit eff  48.64%
15_long_electrons_P>5GeV                          :      1101/     1402  78.53% ( 77.87%),        15 (  1.34%) clones, pur  99.30%, hit eff  48.78%
16_long_electrons_P>3GeV                          :      1487/     2178  68.27% ( 67.86%),        19 (  1.26%) clones, pur  99.22%, hit eff  48.64%
17_long_fromB_electrons                           :        73/      110  66.36% ( 68.72%),         0 (  0.00%) clones, pur  99.73%, hit eff  48.91%
18_long_fromB_electrons_P>5GeV                    :        64/       74  86.49% ( 87.50%),         0 (  0.00%) clones, pur  99.69%, hit eff  48.75%
19_long_PT>2GeV                                   :      1552/     1586  97.86% ( 98.08%),        12 (  0.77%) clones, pur  99.58%, hit eff  49.47%
20_long_from_B_PT>2GeV                            :       597/      605  98.68% ( 98.56%),         2 (  0.33%) clones, pur  99.91%, hit eff  49.65%
21_long_strange_P>5GeV                            :       853/      878  97.15% ( 97.79%),        12 (  1.39%) clones, pur  99.37%, hit eff  48.86%
22_long_strange_P>5GeV_PT>500MeV                  :       327/      335  97.61% ( 97.95%),         4 (  1.21%) clones, pur  99.72%, hit eff  49.03%
23_noVelo+UT+T_fromSignal                         :       107/      168  63.69% ( 64.80%),         1 (  0.93%) clones, pur  99.26%, hit eff  49.69%
24_noVelo+UT+T_fromKs0                            :       946/     1242  76.17% ( 75.71%),        13 (  1.36%) clones, pur  99.36%, hit eff  48.91%
25_noVelo+UT+T_fromLambda                         :       530/      692  76.59% ( 76.76%),         4 (  0.75%) clones, pur  99.29%, hit eff  48.97%
26_noVelo+UT+T_fromSignal_P>5GeV                  :        71/       73  97.26% ( 97.50%),         1 (  1.39%) clones, pur  98.89%, hit eff  49.21%
27_noVelo+UT+T_fromKs0_P>5GeV                     :       672/      685  98.10% ( 98.03%),        11 (  1.61%) clones, pur  99.30%, hit eff  48.94%
28_noVelo+UT+T_fromLambda_P>5GeV                  :       402/      416  96.63% ( 97.01%),         4 (  0.99%) clones, pur  99.17%, hit eff  48.87%
29_noVelo+UT+T_fromSignal_P>5GeV_PT>500MeV        :        50/       52  96.15% ( 96.67%),         1 (  1.96%) clones, pur  98.43%, hit eff  49.06%
30_noVelo+UT+T_fromKs0_P>5GeV_PT>500MeV           :       374/      383  97.65% ( 97.92%),         7 (  1.84%) clones, pur  99.16%, hit eff  49.03%
31_noVelo+UT+T_fromLambda_P>5GeV_PT>500MeV        :       271/      281  96.44% ( 96.61%),         3 (  1.09%) clones, pur  99.33%, hit eff  49.03%
35_long_nSciFiHits_gt_0_AND_lt_5000               :     14052/    17922  78.41% ( 78.27%),       109 (  0.77%) clones, pur  99.56%, hit eff  49.29%
36_long_nSciFiHits_gt_5000_AND_lt_7000            :     12429/    16105  77.17% ( 77.30%),       157 (  1.25%) clones, pur  99.37%, hit eff  48.91%
37_long_nSciFiHits_gt_7000_AND_lt_10000           :      6912/     9149  75.55% ( 75.76%),       107 (  1.52%) clones, pur  99.07%, hit eff  48.53%
38_long_nSciFiHits_gt_10000                       :       187/      449  41.65% ( 41.75%),         1 (  0.53%) clones, pur  98.97%, hit eff  48.65%


selreport_validator validation:
                                               Events  Candidates
Hlt1BGIPseudoPVsBeamOne:                            0           0
Hlt1BGIPseudoPVsBeamTwo:                            0           0
Hlt1BGIPseudoPVsDownBeamBeam:                       0           0
Hlt1BGIPseudoPVsIRBeamBeam:                         1           0
Hlt1BGIPseudoPVsNoBeam:                             0           0
Hlt1BGIPseudoPVsUpBeamBeam:                         0           0
Hlt1BeamGas:                                        0           0
Hlt1ConeJet100GeV:                                  0           0
Hlt1ConeJet15GeV:                                   0           0
Hlt1ConeJet30GeV:                                   0           0
Hlt1ConeJet50GeV:                                   0           0
Hlt1D2KK:                                          15          16
Hlt1D2KPi:                                         22          23
Hlt1D2KPiAlignment:                                 8           8
Hlt1D2Kshh:                                         3           3
Hlt1D2PiPi:                                        11          11
Hlt1DetJpsiToMuMuNegTagLine:                        0           0
Hlt1DetJpsiToMuMuPosTagLine:                        0           0
Hlt1DiElectronDisplaced:                            9          13
Hlt1DiElectronHighMass:                             1           6
Hlt1DiElectronHighMass_SS:                          1           5
Hlt1DiElectronLowMass_SS_massSlice1_displaced:      0           0
Hlt1DiElectronLowMass_SS_massSlice1_prompt:         0           0
Hlt1DiElectronLowMass_SS_massSlice2_displaced:      0           0
Hlt1DiElectronLowMass_SS_massSlice2_prompt:         0           0
Hlt1DiElectronLowMass_SS_massSlice3_displaced:      0           0
Hlt1DiElectronLowMass_SS_massSlice3_prompt:         0           0
Hlt1DiElectronLowMass_SS_massSlice4_displaced:      0           0
Hlt1DiElectronLowMass_SS_massSlice4_prompt:         0           0
Hlt1DiElectronLowMass_massSlice1_displaced:         0           0
Hlt1DiElectronLowMass_massSlice1_prompt:            0           0
Hlt1DiElectronLowMass_massSlice2_displaced:         0           0
Hlt1DiElectronLowMass_massSlice2_prompt:            0           0
Hlt1DiElectronLowMass_massSlice3_displaced:         0           0
Hlt1DiElectronLowMass_massSlice3_prompt:            0           0
Hlt1DiElectronLowMass_massSlice4_displaced:         0           0
Hlt1DiElectronLowMass_massSlice4_prompt:            0           0
Hlt1DiElectronSoft:                                 0           0
Hlt1DiMuonDisplaced:                                9          12
Hlt1DiMuonDrellYan:                                 0           0
Hlt1DiMuonDrellYan_SS:                              0           0
Hlt1DiMuonDrellYan_VLowMass:                        0           0
Hlt1DiMuonDrellYan_VLowMass_SS:                     0           0
Hlt1DiMuonHighMass:                                 8          12
Hlt1DiMuonJpsiMassAlignment:                        0           0
Hlt1DiMuonNoIP:                                     0           0
Hlt1DiMuonNoIP_SS:                                  0           0
Hlt1DiMuonSoft:                                     0           0
Hlt1DiPhotonHighMass:                               3           6
Hlt1Dst2D0Pi:                                       0           0
Hlt1Dst2D0PiAlignment:                              0           0
Hlt1ErrorBank:                                      0           0
Hlt1GECPassthrough:                                 0           0
Hlt1KsToPiPi:                                       0           0
Hlt1KsToPiPiDoubleMuonMisID:                        0           0
Hlt1LambdaLLDetachedTrack:                          2           2
Hlt1MaterialVertexSeedsDownstreamz:                 0           0
Hlt1MaterialVertexSeeds_DWFS:                       0           0
Hlt1ODIN1kHzLumi:                                   0           0
Hlt1ODINCalib:                                      0           0
Hlt1ODINLumi:                                       0           0
Hlt1ODINeeFarFromActivity:                          0           0
Hlt1OneMuonTrackLine:                               1           0
Hlt1Passthrough:                                    0           0
Hlt1PassthroughPVinSMOG2:                           0           0
Hlt1Pi02GammaGamma:                                 0           0
Hlt1RICH1Alignment:                                 0           0
Hlt1RICH2Alignment:                                 0           0
Hlt1SMOG22BodyGeneric:                              0           0
Hlt1SMOG22BodyGenericPrompt:                        0           0
Hlt1SMOG2BELowMultElectrons:                        0           0
Hlt1SMOG2BENoBias:                                  0           0
Hlt1SMOG2D2Kpi:                                     0           0
Hlt1SMOG2DiMuonHighMass:                            0           0
Hlt1SMOG2DisplacedDiMuon:                           0           0
Hlt1SMOG2JPsiToMuMuTaP_NegTag:                      0           0
Hlt1SMOG2JPsiToMuMuTaP_PosTag:                      0           0
Hlt1SMOG2KsTopipi:                                  0           0
Hlt1SMOG2L0Toppi:                                   0           0
Hlt1SMOG2MinimumBias:                               0           0
Hlt1SMOG2PassThroughLowMult5:                       0           0
Hlt1SMOG2SingleMuon:                                0           0
Hlt1SMOG2SingleTrackHighPt:                         0           0
Hlt1SMOG2SingleTrackVeryHighPt:                     0           0
Hlt1SMOG2etacTopp:                                  0           0
Hlt1SingleHighPtElectron:                           5          13
Hlt1SingleHighPtMuon:                               1           1
Hlt1SingleHighPtMuonNoMuID:                         4           4
Hlt1TAEPassthrough:                                 0           0
Hlt1TrackElectronMVA:                              23          23
Hlt1TrackMVA:                                     121         193
Hlt1TrackMuonMVA:                                   9           9
Hlt1TwoKs:                                          0           0
Hlt1TwoTrackKs:                                     2           3
Hlt1TwoTrackMVA:                                  286         998
Hlt1UpsilonAlignment:                               0           0
Hlt1VeloMicroBias:                                  1           0
Hlt1VeloMicroBiasVeloClosing:                       0           0
Hlt1XiOmegaLLL:                                     0           0

Total decisions:      546
Total tracks:         1147
Total calos clusters: 9
Total SVs:            1075
Total hits:           29905
Total stdinfo:        14058


velo_validator validation:
TrackChecker output                               :      3240/   153803   2.11% ghosts
01_velo                                           :     65023/    66132  98.32% ( 98.47%),      1896 (  2.83%) clones, pur  99.66%, hit eff  95.40%
02_long                                           :     37665/    37926  99.31% ( 99.36%),       863 (  2.24%) clones, pur  99.78%, hit eff  96.61%
03_long_P>5GeV                                    :     24615/    24714  99.60% ( 99.63%),       520 (  2.07%) clones, pur  99.81%, hit eff  97.16%
04_long_strange                                   :      1754/     1807  97.07% ( 97.72%),        37 (  2.07%) clones, pur  99.42%, hit eff  96.31%
05_long_strange_P>5GeV                            :       852/      878  97.04% ( 97.01%),        14 (  1.62%) clones, pur  99.29%, hit eff  96.80%
06_long_fromB                                     :      2288/     2309  99.09% ( 99.30%),        45 (  1.93%) clones, pur  99.71%, hit eff  97.02%
07_long_fromB_P>5GeV                              :      1900/     1909  99.53% ( 99.60%),        36 (  1.86%) clones, pur  99.80%, hit eff  97.25%
08_long_electrons                                 :      2613/     2697  96.89% ( 97.46%),       116 (  4.25%) clones, pur  97.86%, hit eff  95.41%
09_long_fromB_electrons                           :       107/      110  97.27% ( 97.53%),         5 (  4.46%) clones, pur  98.51%, hit eff  95.24%
10_long_fromB_electrons_P>5GeV                    :        72/       74  97.30% ( 98.44%),         2 (  2.70%) clones, pur  98.11%, hit eff  96.48%
11_long_fromSignal                                :      1300/     1312  99.09% ( 99.09%),        22 (  1.66%) clones, pur  99.70%, hit eff  97.24%

