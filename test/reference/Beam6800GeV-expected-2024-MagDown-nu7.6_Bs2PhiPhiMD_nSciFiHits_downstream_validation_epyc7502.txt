downstream_validator validation:
TrackChecker output                               :       784/     4555  17.21% ghosts
for P>3GeV,Pt>0.5GeV                              :       321/     2092  15.34% ghosts
01_UT+T                                           :      2801/    42859   6.54% (  6.74%),         0 (  0.00%) clones, pur  98.28%, hit eff  96.23%
02_UT+T_P>5GeV                                    :      1937/    26564   7.29% (  7.51%),         0 (  0.00%) clones, pur  98.18%, hit eff  96.51%
03_UT+T_strange                                   :       908/     3761  24.14% ( 24.20%),         0 (  0.00%) clones, pur  98.85%, hit eff  97.35%
04_UT+T_strange_P>5GeV                            :       705/     1970  35.79% ( 36.58%),         0 (  0.00%) clones, pur  98.86%, hit eff  97.72%
05_noVelo+UT+T_strange                            :       821/     1984  41.38% ( 43.42%),         0 (  0.00%) clones, pur  98.84%, hit eff  97.44%
06_noVelo+UT+T_strange_P>5GeV                     :       647/     1114  58.08% ( 60.55%),         0 (  0.00%) clones, pur  98.84%, hit eff  97.76%
07_UT+T_fromDB                                    :       177/     3077   5.75% (  5.34%),         0 (  0.00%) clones, pur  98.04%, hit eff  96.13%
08_UT+T_fromBD_P>5GeV                             :       115/     2337   4.92% (  4.41%),         0 (  0.00%) clones, pur  97.82%, hit eff  96.06%
09_noVelo+UT+T_fromBD                             :       114/      359  31.75% ( 34.50%),         0 (  0.00%) clones, pur  98.65%, hit eff  97.40%
10_noVelo+UT+T_fromBD_P>5GeV                      :        78/      151  51.66% ( 52.48%),         0 (  0.00%) clones, pur  98.60%, hit eff  97.33%
11_UT+T_SfromDB                                   :        54/      181  29.83% ( 28.15%),         0 (  0.00%) clones, pur  98.83%, hit eff  97.48%
12_UT+T_SfromDB_P>5GeV                            :        41/      101  40.59% ( 38.65%),         0 (  0.00%) clones, pur  98.61%, hit eff  97.44%
13_noVelo+UT+T_SfromDB                            :        50/       98  51.02% ( 50.83%),         0 (  0.00%) clones, pur  98.73%, hit eff  97.51%
14_noVelo+UT+T_SfromDB_P>5GeV                     :        39/       53  73.58% ( 75.44%),         0 (  0.00%) clones, pur  98.53%, hit eff  97.46%
15_noVelo+UT+T_fromSignal                         :        45/      168  26.79% ( 31.71%),         0 (  0.00%) clones, pur  98.29%, hit eff  97.09%
16_noVelo+UT+T_fromKs0                            :       494/     1242  39.77% ( 40.50%),         0 (  0.00%) clones, pur  98.96%, hit eff  97.42%
17_noVelo+UT+T_fromLambda                         :       335/      692  48.41% ( 49.06%),         0 (  0.00%) clones, pur  98.63%, hit eff  97.43%
19_noVelo+UT+T_fromSignal_P>5GeV                  :        31/       73  42.47% ( 41.33%),         0 (  0.00%) clones, pur  98.55%, hit eff  97.37%
20_noVelo+UT+T_fromKs0_P>5GeV                     :       385/      685  56.20% ( 57.94%),         0 (  0.00%) clones, pur  99.03%, hit eff  97.82%
21_noVelo+UT+T_fromLambda_P>5GeV                  :       279/      416  67.07% ( 68.18%),         0 (  0.00%) clones, pur  98.50%, hit eff  97.53%
22_noVelo+UT+T_fromSignal_P>5GeV_PT>500MeV        :        25/       52  48.08% ( 47.92%),         0 (  0.00%) clones, pur  98.70%, hit eff  97.24%
23_noVelo+UT+T_fromKs0_P>5GeV_PT>500MeV           :       278/      383  72.58% ( 73.08%),         0 (  0.00%) clones, pur  98.91%, hit eff  97.88%
24_noVelo+UT+T_fromLambda_P>5GeV_PT>500MeV        :       214/      281  76.16% ( 74.70%),         0 (  0.00%) clones, pur  98.52%, hit eff  97.53%


long_validator validation:
TrackChecker output                               :      1478/    30154   4.90% ghosts
for P>3GeV,Pt>0.5GeV                              :       372/    15951   2.33% ghosts
01_long                                           :     26572/    37926  70.06% ( 70.68%),       555 (  2.05%) clones, pur  99.76%, hit eff  98.22%
02_long_P>5GeV                                    :     21494/    24714  86.97% ( 87.72%),       463 (  2.11%) clones, pur  99.77%, hit eff  98.51%
03_long_strange                                   :      1018/     1807  56.34% ( 56.54%),        17 (  1.64%) clones, pur  99.63%, hit eff  97.92%
04_long_strange_P>5GeV                            :       708/      878  80.64% ( 80.22%),        12 (  1.67%) clones, pur  99.65%, hit eff  98.24%
05_long_fromB                                     :      1933/     2309  83.72% ( 84.31%),        41 (  2.08%) clones, pur  99.79%, hit eff  98.57%
06_long_fromB_P>5GeV                              :      1777/     1909  93.09% ( 93.20%),        39 (  2.15%) clones, pur  99.80%, hit eff  98.70%
07_long_electrons                                 :       907/     2697  33.63% ( 33.42%),        32 (  3.41%) clones, pur  99.18%, hit eff  98.20%
08_long_electrons_P>5GeV                          :       711/     1402  50.71% ( 51.41%),        29 (  3.92%) clones, pur  99.17%, hit eff  98.42%
09_long_fromB_electrons                           :        52/      110  47.27% ( 49.38%),         0 (  0.00%) clones, pur  99.56%, hit eff  98.77%
10_long_fromB_electrons_P>5GeV                    :        47/       74  63.51% ( 65.62%),         0 (  0.00%) clones, pur  99.64%, hit eff  98.81%
long_P>5GeV_AND_Pt>1GeV                           :      5903/     6486  91.01% ( 91.78%),       139 (  2.30%) clones, pur  99.75%, hit eff  98.57%
long_fromB_P>5GeV_AND_Pt>1GeV                     :      1205/     1281  94.07% ( 94.14%),        26 (  2.11%) clones, pur  99.77%, hit eff  98.73%
11_noVelo_UT                                      :         0/     4124   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
12_noVelo_UT_P>5GeV                               :         0/     1671   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
13_long_PT>2GeV                                   :      1493/     1586  94.14% ( 93.84%),        23 (  1.52%) clones, pur  99.79%, hit eff  98.90%
14_long_from_B_PT>2GeV                            :       585/      605  96.69% ( 96.52%),         5 (  0.85%) clones, pur  99.79%, hit eff  98.94%
15_long_strange_P>5GeV                            :       708/      878  80.64% ( 80.22%),        12 (  1.67%) clones, pur  99.65%, hit eff  98.24%
16_long_strange_P>5GeV_PT>500MeV                  :       295/      335  88.06% ( 87.79%),         5 (  1.67%) clones, pur  99.76%, hit eff  98.28%
17_long_fromSignal                                :      1186/     1312  90.40% ( 90.13%),        26 (  2.15%) clones, pur  99.76%, hit eff  98.62%
18_long_nSciFiHits_gt_0_AND_lt_5000               :     11591/    16638  69.67% ( 69.52%),       258 (  2.18%) clones, pur  99.82%, hit eff  98.67%
19_long_nSciFiHits_gt_5000_AND_lt_7000            :     10124/    14945  67.74% ( 67.97%),       195 (  1.89%) clones, pur  99.72%, hit eff  98.09%
20_long_nSciFiHits_gt_7000_AND_lt_10000           :      5521/     8507  64.90% ( 65.12%),       127 (  2.25%) clones, pur  99.61%, hit eff  97.52%
21_long_nSciFiHits_gt_10000                       :       145/      409  35.45% ( 35.53%),         4 (  2.68%) clones, pur  99.44%, hit eff  97.07%


seed_validator validation:
TrackChecker output                               :      4446/    52279   8.50% ghosts
for P>3GeV,Pt>0.5GeV                              :         0/        0   -nan% ghosts
00_P>3Gev_Pt>0.5                                  :     15457/    17038  90.72% ( 91.47%),       230 (  1.47%) clones, pur  99.32%, hit eff  97.71%
01_long                                           :     29448/    37926  77.65% ( 78.12%),       405 (  1.36%) clones, pur  99.38%, hit eff  97.74%
---1. phi quadrant                                :      7319/     9440  77.53% ( 77.86%),       124 (  1.67%) clones, pur  99.30%, hit eff  97.70%
---2. phi quadrant                                :      7484/     9611  77.87% ( 78.65%),        95 (  1.25%) clones, pur  99.39%, hit eff  97.70%
---3. phi quadrant                                :      7288/     9375  77.74% ( 77.86%),        89 (  1.21%) clones, pur  99.40%, hit eff  97.78%
---4. phi quadrant                                :      7356/     9499  77.44% ( 77.56%),        97 (  1.30%) clones, pur  99.41%, hit eff  97.78%
---eta < 2.5, small x, large y                    :       509/     1614  31.54% ( 30.45%),         4 (  0.78%) clones, pur  98.87%, hit eff  94.97%
---eta < 2.5, large x, small y                    :      1473/     2835  51.96% ( 51.56%),        26 (  1.73%) clones, pur  99.15%, hit eff  96.66%
---eta > 2.5, small x, large y                    :     10047/    12171  82.55% ( 83.11%),       135 (  1.33%) clones, pur  99.37%, hit eff  97.73%
---eta > 2.5, large x, small y                    :     17419/    21306  81.76% ( 81.97%),       240 (  1.36%) clones, pur  99.41%, hit eff  97.92%
02_long_P>5GeV                                    :     23304/    24714  94.29% ( 95.02%),       347 (  1.47%) clones, pur  99.37%, hit eff  98.02%
02_long_P>5GeV, eta > 4                           :      9337/     9981  93.55% ( 94.44%),       147 (  1.55%) clones, pur  99.27%, hit eff  97.80%
---eta < 2.5, small x, large y                    :       394/      479  82.25% ( 82.00%),         3 (  0.76%) clones, pur  99.17%, hit eff  97.05%
---eta < 2.5, large x, small y                    :       907/      952  95.27% ( 95.66%),        15 (  1.63%) clones, pur  99.59%, hit eff  98.37%
---eta > 2.5, small x, large y                    :      8228/     8612  95.54% ( 96.22%),       120 (  1.44%) clones, pur  99.35%, hit eff  98.04%
---eta > 2.5, large x, small y                    :     13775/    14671  93.89% ( 94.56%),       209 (  1.49%) clones, pur  99.38%, hit eff  98.01%
03_long_P>3GeV                                    :     29443/    32393  90.89% ( 91.52%),       405 (  1.36%) clones, pur  99.38%, hit eff  97.74%
04_long_P>0.5GeV                                  :     29448/    37926  77.65% ( 78.12%),       405 (  1.36%) clones, pur  99.38%, hit eff  97.74%
05_long_from_B                                    :      2013/     2309  87.18% ( 87.86%),        23 (  1.13%) clones, pur  99.54%, hit eff  98.24%
06_long_from_B_P>5GeV                             :      1830/     1909  95.86% ( 95.94%),        21 (  1.13%) clones, pur  99.58%, hit eff  98.43%
07_long_from_B_P>3GeV                             :      2013/     2155  93.41% ( 93.15%),        23 (  1.13%) clones, pur  99.54%, hit eff  98.24%
08_UT+SciFi                                       :      3135/     5429  57.75% ( 56.79%),        32 (  1.01%) clones, pur  99.30%, hit eff  96.88%
09_UT+SciFi_P>5GeV                                :      2044/     2268  90.12% ( 91.03%),        24 (  1.16%) clones, pur  99.31%, hit eff  97.32%
10_UT+SciFi_P>3GeV                                :      3123/     3817  81.82% ( 82.18%),        32 (  1.01%) clones, pur  99.31%, hit eff  96.91%
11_UT+SciFi_fromStrange                           :      1433/     1984  72.23% ( 72.31%),        15 (  1.04%) clones, pur  99.52%, hit eff  97.64%
12_UT+SciFi_fromStrange_P>5GeV                    :      1049/     1114  94.17% ( 95.09%),        13 (  1.22%) clones, pur  99.52%, hit eff  97.95%
13_UT+SciFi_fromStrange_P>3GeV                    :      1432/     1613  88.78% ( 89.48%),        15 (  1.04%) clones, pur  99.52%, hit eff  97.65%
14_long_electrons                                 :      1446/     2697  53.62% ( 52.44%),        19 (  1.30%) clones, pur  99.42%, hit eff  97.64%
15_long_electrons_P>5GeV                          :      1077/     1402  76.82% ( 76.04%),        16 (  1.46%) clones, pur  99.51%, hit eff  97.90%
16_long_electrons_P>3GeV                          :      1446/     2178  66.39% ( 65.84%),        19 (  1.30%) clones, pur  99.42%, hit eff  97.64%
17_long_fromB_electrons                           :        72/      110  65.45% ( 67.49%),         1 (  1.37%) clones, pur  99.24%, hit eff  97.86%
18_long_fromB_electrons_P>5GeV                    :        62/       74  83.78% ( 85.16%),         1 (  1.59%) clones, pur  99.68%, hit eff  98.18%
19_long_PT>2GeV                                   :      1525/     1586  96.15% ( 96.31%),        17 (  1.10%) clones, pur  99.54%, hit eff  98.53%
20_long_from_B_PT>2GeV                            :       595/      605  98.35% ( 97.88%),         3 (  0.50%) clones, pur  99.79%, hit eff  98.83%
21_long_strange_P>5GeV                            :       827/      878  94.19% ( 94.90%),        12 (  1.43%) clones, pur  99.35%, hit eff  97.70%
22_long_strange_P>5GeV_PT>500MeV                  :       321/      335  95.82% ( 96.05%),         5 (  1.53%) clones, pur  99.57%, hit eff  97.78%
23_noVelo+UT+T_fromSignal                         :        93/      168  55.36% ( 56.67%),         2 (  2.11%) clones, pur  98.52%, hit eff  96.01%
24_noVelo+UT+T_fromKs0                            :       909/     1242  73.19% ( 72.29%),        10 (  1.09%) clones, pur  99.55%, hit eff  97.56%
25_noVelo+UT+T_fromLambda                         :       499/      692  72.11% ( 72.74%),         5 (  0.99%) clones, pur  99.45%, hit eff  97.80%
26_noVelo+UT+T_fromSignal_P>5GeV                  :        62/       73  84.93% ( 84.83%),         1 (  1.59%) clones, pur  98.36%, hit eff  96.65%
27_noVelo+UT+T_fromKs0_P>5GeV                     :       651/      685  95.04% ( 95.01%),         8 (  1.21%) clones, pur  99.58%, hit eff  98.08%
28_noVelo+UT+T_fromLambda_P>5GeV                  :       386/      416  92.79% ( 93.35%),         5 (  1.28%) clones, pur  99.36%, hit eff  97.80%
29_noVelo+UT+T_fromSignal_P>5GeV_PT>500MeV        :        43/       52  82.69% ( 81.67%),         1 (  2.27%) clones, pur  97.66%, hit eff  95.39%
30_noVelo+UT+T_fromKs0_P>5GeV_PT>500MeV           :       360/      383  93.99% ( 94.22%),         3 (  0.83%) clones, pur  99.62%, hit eff  98.34%
31_noVelo+UT+T_fromLambda_P>5GeV_PT>500MeV        :       261/      281  92.88% ( 93.08%),         4 (  1.51%) clones, pur  99.51%, hit eff  97.89%
35_long_nSciFiHits_gt_0_AND_lt_5000               :     13621/    17922  76.00% ( 75.82%),       142 (  1.03%) clones, pur  99.58%, hit eff  98.29%
36_long_nSciFiHits_gt_5000_AND_lt_7000            :     12042/    16105  74.77% ( 74.92%),       179 (  1.46%) clones, pur  99.28%, hit eff  97.50%
37_long_nSciFiHits_gt_7000_AND_lt_10000           :      6651/     9149  72.70% ( 72.85%),       127 (  1.87%) clones, pur  99.05%, hit eff  96.85%
38_long_nSciFiHits_gt_10000                       :       177/      449  39.42% ( 39.49%),         2 (  1.12%) clones, pur  98.98%, hit eff  96.19%


unmached_seed_validator validation:
TrackChecker output                               :      4267/    23083  18.49% ghosts
for P>3GeV,Pt>0.5GeV                              :         0/        0   -nan% ghosts
00_P>3Gev_Pt>0.5                                  :       872/    17038   5.12% (  5.03%),        12 (  1.36%) clones, pur  95.68%, hit eff  90.79%
01_long                                           :      2864/    37926   7.55% (  7.46%),        33 (  1.14%) clones, pur  97.48%, hit eff  94.16%
---1. phi quadrant                                :       747/     9440   7.91% (  7.56%),        10 (  1.32%) clones, pur  97.17%, hit eff  93.88%
---2. phi quadrant                                :       739/     9611   7.69% (  7.82%),         4 (  0.54%) clones, pur  97.66%, hit eff  94.32%
---3. phi quadrant                                :       702/     9375   7.49% (  7.56%),         8 (  1.13%) clones, pur  97.61%, hit eff  94.48%
---4. phi quadrant                                :       676/     9499   7.12% (  7.02%),        11 (  1.60%) clones, pur  97.50%, hit eff  93.99%
---eta < 2.5, small x, large y                    :        69/     1614   4.28% (  4.26%),         0 (  0.00%) clones, pur  97.24%, hit eff  89.06%
---eta < 2.5, large x, small y                    :       124/     2835   4.37% (  4.29%),         2 (  1.59%) clones, pur  96.24%, hit eff  90.49%
---eta > 2.5, small x, large y                    :       929/    12171   7.63% (  7.49%),        12 (  1.28%) clones, pur  97.12%, hit eff  93.83%
---eta > 2.5, large x, small y                    :      1742/    21306   8.18% (  8.12%),        19 (  1.08%) clones, pur  97.77%, hit eff  94.80%
02_long_P>5GeV                                    :      1869/    24714   7.56% (  7.58%),        25 (  1.32%) clones, pur  96.79%, hit eff  93.47%
02_long_P>5GeV, eta > 4                           :      1066/     9981  10.68% ( 10.76%),        14 (  1.30%) clones, pur  96.99%, hit eff  94.03%
---eta < 2.5, small x, large y                    :        38/      479   7.93% (  8.41%),         0 (  0.00%) clones, pur  97.07%, hit eff  91.23%
---eta < 2.5, large x, small y                    :        51/      952   5.36% (  4.72%),         0 (  0.00%) clones, pur  97.90%, hit eff  94.18%
---eta > 2.5, small x, large y                    :       647/     8612   7.51% (  7.42%),        11 (  1.67%) clones, pur  96.25%, hit eff  93.00%
---eta > 2.5, large x, small y                    :      1133/    14671   7.72% (  7.79%),        14 (  1.22%) clones, pur  97.04%, hit eff  93.78%
03_long_P>3GeV                                    :      2863/    32393   8.84% (  8.74%),        33 (  1.14%) clones, pur  97.48%, hit eff  94.16%
04_long_P>0.5GeV                                  :      2864/    37926   7.55% (  7.46%),        33 (  1.14%) clones, pur  97.48%, hit eff  94.16%
05_long_from_B                                    :        87/     2309   3.77% (  3.78%),         1 (  1.14%) clones, pur  96.21%, hit eff  92.22%
06_long_from_B_P>5GeV                             :        60/     1909   3.14% (  2.99%),         0 (  0.00%) clones, pur  95.56%, hit eff  91.32%
07_long_from_B_P>3GeV                             :        87/     2155   4.04% (  4.08%),         1 (  1.14%) clones, pur  96.21%, hit eff  92.22%
08_UT+SciFi                                       :      2998/     5429  55.22% ( 54.44%),        26 (  0.86%) clones, pur  99.31%, hit eff  96.87%
09_UT+SciFi_P>5GeV                                :      1964/     2268  86.60% ( 87.08%),        18 (  0.91%) clones, pur  99.34%, hit eff  97.36%
10_UT+SciFi_P>3GeV                                :      2989/     3817  78.31% ( 78.82%),        26 (  0.86%) clones, pur  99.33%, hit eff  96.90%
11_UT+SciFi_fromStrange                           :      1377/     1984  69.41% ( 69.53%),        14 (  1.01%) clones, pur  99.51%, hit eff  97.61%
12_UT+SciFi_fromStrange_P>5GeV                    :      1014/     1114  91.02% ( 91.44%),        12 (  1.17%) clones, pur  99.52%, hit eff  97.96%
13_UT+SciFi_fromStrange_P>3GeV                    :      1376/     1613  85.31% ( 86.04%),        14 (  1.01%) clones, pur  99.51%, hit eff  97.62%
14_long_electrons                                 :       404/     2697  14.98% ( 14.23%),         3 (  0.74%) clones, pur  98.85%, hit eff  96.53%
15_long_electrons_P>5GeV                          :       269/     1402  19.19% ( 17.57%),         2 (  0.74%) clones, pur  98.99%, hit eff  96.87%
16_long_electrons_P>3GeV                          :       404/     2178  18.55% ( 17.92%),         3 (  0.74%) clones, pur  98.85%, hit eff  96.53%
17_long_fromB_electrons                           :        17/      110  15.45% ( 14.40%),         0 (  0.00%) clones, pur  98.40%, hit eff  97.01%
18_long_fromB_electrons_P>5GeV                    :        13/       74  17.57% ( 16.41%),         0 (  0.00%) clones, pur 100.00%, hit eff  98.66%
19_long_PT>2GeV                                   :        42/     1586   2.65% (  3.02%),         1 (  2.33%) clones, pur  94.08%, hit eff  88.85%
20_long_from_B_PT>2GeV                            :        11/      605   1.82% (  1.68%),         0 (  0.00%) clones, pur  98.18%, hit eff  93.35%
21_long_strange_P>5GeV                            :       116/      878  13.21% ( 14.04%),         2 (  1.69%) clones, pur  98.54%, hit eff  95.54%
22_long_strange_P>5GeV_PT>500MeV                  :        24/      335   7.16% (  7.53%),         1 (  4.00%) clones, pur  98.11%, hit eff  93.79%
23_noVelo+UT+T_fromSignal                         :        89/      168  52.98% ( 55.41%),         2 (  2.20%) clones, pur  98.90%, hit eff  96.28%
24_noVelo+UT+T_fromKs0                            :       870/     1242  70.05% ( 69.45%),        10 (  1.14%) clones, pur  99.55%, hit eff  97.51%
25_noVelo+UT+T_fromLambda                         :       482/      692  69.65% ( 69.50%),         3 (  0.62%) clones, pur  99.43%, hit eff  97.76%
26_noVelo+UT+T_fromSignal_P>5GeV                  :        58/       73  79.45% ( 82.17%),         1 (  1.69%) clones, pur  98.93%, hit eff  97.10%
27_noVelo+UT+T_fromKs0_P>5GeV                     :       627/      685  91.53% ( 91.67%),         8 (  1.26%) clones, pur  99.59%, hit eff  98.07%
28_noVelo+UT+T_fromLambda_P>5GeV                  :       376/      416  90.38% ( 90.48%),         3 (  0.79%) clones, pur  99.34%, hit eff  97.78%
29_noVelo+UT+T_fromSignal_P>5GeV_PT>500MeV        :        39/       52  75.00% ( 77.92%),         1 (  2.50%) clones, pur  98.42%, hit eff  95.93%
30_noVelo+UT+T_fromKs0_P>5GeV_PT>500MeV           :       354/      383  92.43% ( 92.51%),         3 (  0.84%) clones, pur  99.61%, hit eff  98.34%
31_noVelo+UT+T_fromLambda_P>5GeV_PT>500MeV        :       254/      281  90.39% ( 89.99%),         2 (  0.78%) clones, pur  99.49%, hit eff  97.88%
35_long_nSciFiHits_gt_0_AND_lt_5000               :      1879/    17922  10.48% ( 10.57%),        18 (  0.95%) clones, pur  98.66%, hit eff  96.35%
36_long_nSciFiHits_gt_5000_AND_lt_7000            :      1764/    16105  10.95% ( 10.89%),        28 (  1.56%) clones, pur  97.75%, hit eff  94.78%
37_long_nSciFiHits_gt_7000_AND_lt_10000           :      1053/     9149  11.51% ( 11.43%),        14 (  1.31%) clones, pur  97.41%, hit eff  93.94%
38_long_nSciFiHits_gt_10000                       :        25/      449   5.57% (  5.58%),         0 (  0.00%) clones, pur  96.47%, hit eff  90.79%

