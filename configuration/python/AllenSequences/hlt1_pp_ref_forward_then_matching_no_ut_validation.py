###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the Apache License          #
# version 2 (Apache-2.0), copied verbatim in the file "COPYING".              #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from AllenConf.HLT1_pp_ref import setup_hlt1_node
from AllenCore.generator import generate
from AllenConf.enum_types import TrackingType
from AllenConf.get_thresholds import get_thresholds

hlt1_node = setup_hlt1_node(
    withMCChecking=True,
    tracking_type=TrackingType.FORWARD_THEN_MATCHING,
    threshold_settings=get_thresholds("pp_reference_run_2024"),
    with_ut=False)
generate(hlt1_node)
