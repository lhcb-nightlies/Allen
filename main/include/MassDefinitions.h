/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the Apache License          *
* version 2 (Apache-2.0), copied verbatim in the file "LICENSE".              *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

namespace Allen {

  constexpr float mEl = 0.51099891f;
  constexpr float mMu = 105.65837f;
  constexpr float mPi = 139.57018f;
  constexpr float mK = 493.677f;
  constexpr float mP = 938.27203f;
  constexpr float mL = 1115.683f;
  constexpr float mXi = 1321.71f;
  constexpr float mOmega = 1672.45f;
  constexpr float mDz = 1864.83f;
} // namespace Allen
