/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the Apache License          *
* version 2 (Apache-2.0), copied verbatim in the file "LICENSE".              *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DUMPFTHITS_H
#define DUMPFTHITS_H 1

#include <cstring>
#include <fstream>
#include <string>
#include <vector>

// Include files
#include "Event/ODIN.h"
#include "GaudiAlg/Consumer.h"
#include "Event/PrHits.h"

/** @class DumpFTHits DumpFTHits.h
 *  Algorithm that dumps FT hit variables to binary files.
 *
 *  @author Roel Aaij
 *  @date   2018-08-27
 */
class DumpFTHits : public Gaudi::Functional::Consumer<void(const LHCb::ODIN&, const LHCb::Pr::FT::Hits&)> {
public:
  /// Standard constructor
  DumpFTHits(const std::string& name, ISvcLocator* pSvcLocator);

  StatusCode initialize() override;

  void operator()(const LHCb::ODIN& odin, const LHCb::Pr::FT::Hits& ftHits) const override;

private:
  Gaudi::Property<std::string> m_outputDirectory {this, "OutputDirectory", "scifi_hits"};
};
#endif // DUMPFTHITS_H
