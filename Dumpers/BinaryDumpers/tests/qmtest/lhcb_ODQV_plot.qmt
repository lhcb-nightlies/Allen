<?xml version="1.0" ?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the Apache License          #
# version 2 (Apache-2.0), copied verbatim in the file "LICENSE".              #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
-->
<!--
#######################################################
# SUMMARY OF THIS TEST
# ...................
# Author: Andy Morris
# Purpose: Plot the output of ODQV using the
#          associated plotting script
#######################################################
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
  <argument name="program"><text>root</text></argument>
  <argument name="args"><set>
    <text>-l</text>
    <text>-x</text>
    <text>-b</text>
    <text>-q</text>
    <text>$ALLEN_PROJECT_ROOT/scripts/DataQualityPlot_Overlay.cc</text>
  </set></argument>
  <argument name="prerequisites"><set>
    <tuple><text>BinaryDumpers.lhcb_ODQV</text><enumeral>PASS</enumeral></tuple>
  </set></argument>
  <argument name="use_temp_dir"><enumeral>true</enumeral></argument>
  <argument name="timeout"><integer>120</integer></argument>
  <argument name="validator"><text>

import os

if not os.path.exists("allen_odqv_qmtest.root"):
    causes.append("Couldn't find input file allen_odqv_qmtest.root from the prerequisite job!\n")

expectedFiles=["fileCanvas.pdf", "IPforwardCanvas.pdf",
        "IPmatchingCanvas.pdf", "kalmanCovCanvas.pdf",
        "longForwardCanvas.pdf", "longMatchingCanvas.pdf",
        "occupancyCanvas.pdf", "PIDCanvas.pdf",
        "PIDkinCanvas.pdf", "PVcanvas.pdf",
        "PVcovCanvas.pdf", "PVdistCanvas.pdf",
	"IPresolutionCanvas.pdf", "veloCanvas.pdf"]

for file in expectedFiles:
    if not os.path.exists(file):
        causes.append("could not find expected output file: " + file + "\n")

  </text></argument>
</extension>
