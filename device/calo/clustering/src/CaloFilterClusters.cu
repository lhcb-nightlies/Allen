/*****************************************************************************\
* (c) Copyright 2018-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the Apache License          *
* version 2 (Apache-2.0), copied verbatim in the file "LICENSE".              *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFilterClusters.cuh"

INSTANTIATE_ALGORITHM(calo_filter_clusters::calo_filter_clusters_t)

void calo_filter_clusters::calo_filter_clusters_t::set_arguments_size(
  ArgumentReferences<Parameters> arguments,
  const RuntimeOptions&,
  const Constants&) const
{
  set_size<dev_cluster1_idx_t>(arguments, first<host_ecal_number_of_twoclusters_t>(arguments));
  set_size<dev_cluster2_idx_t>(arguments, first<host_ecal_number_of_twoclusters_t>(arguments));
}

void calo_filter_clusters::calo_filter_clusters_t::operator()(
  const ArgumentReferences<Parameters>& arguments,
  const RuntimeOptions&,
  const Constants&,
  const Allen::Context& context) const
{
  global_function(calo_filter_clusters)(dim3(size<dev_event_list_t>(arguments)), m_block_dim_filter, context)(
    arguments, m_calo_clusters.data(context));
}

__global__ void calo_filter_clusters::calo_filter_clusters(
  calo_filter_clusters::Parameters parameters,
  Allen::Monitoring::AveragingCounter<>::DeviceType device_counter)
{
  const unsigned event_number = parameters.dev_event_list[blockIdx.x];

  const unsigned ecal_twoclusters_offsets = parameters.dev_ecal_twocluster_offsets[event_number];
  unsigned* event_cluster1_idx = parameters.dev_cluster1_idx + ecal_twoclusters_offsets;
  unsigned* event_cluster2_idx = parameters.dev_cluster2_idx + ecal_twoclusters_offsets;

  const auto event_neutral_particles = parameters.dev_neutral_particles->container(event_number);
  const unsigned* prefiltered_clusters_idx = parameters.dev_prefiltered_clusters_idx + event_neutral_particles.offset();
  const unsigned n_prefltred_clusters = parameters.dev_num_prefiltered_clusters[event_number];

  // Loop over pre-filtered clusters.
  for (unsigned i_cluster = threadIdx.x; i_cluster < n_prefltred_clusters; i_cluster += blockDim.x) {
    const unsigned dicluster_idx_offset = i_cluster * n_prefltred_clusters - ((i_cluster + 1) * i_cluster) / 2;
    for (unsigned j_cluster = threadIdx.y + i_cluster + 1; j_cluster < n_prefltred_clusters; j_cluster += blockDim.y) {
      const unsigned dicluster_idx = dicluster_idx_offset + j_cluster - i_cluster - 1;
      event_cluster1_idx[dicluster_idx] = prefiltered_clusters_idx[i_cluster];
      event_cluster2_idx[dicluster_idx] = prefiltered_clusters_idx[j_cluster];
    }
  }

  const unsigned n_clusters_event =
    parameters.dev_ecal_cluster_offsets[event_number + 1] - parameters.dev_ecal_cluster_offsets[event_number];
  device_counter.add(n_clusters_event);
}
