/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the Apache License          *
* version 2 (Apache-2.0), copied verbatim in the file "LICENSE".              *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "HostUnmatchedSeedingValidator.h"
#include "PrepareTracks.h"

INSTANTIATE_ALGORITHM(host_unmatched_seeding_validator::host_unmatched_seeding_validator_t);
void host_unmatched_seeding_validator::host_unmatched_seeding_validator_t::operator()(
  const ArgumentReferences<Parameters>& arguments,
  const RuntimeOptions& runtime_options,
  const Constants&,
  const Allen::Context& context) const
{
  const auto scifi_seed_atomics = make_host_buffer<dev_offsets_scifi_seeds_t>(arguments, context);
  const auto scifi_seed_hit_number = make_host_buffer<dev_offsets_scifi_seed_hit_number_t>(arguments, context);
  const auto scifi_seed_hits = make_host_buffer<dev_scifi_hits_t>(arguments, context);
  const auto scifi_seeds = make_host_buffer<dev_scifi_seeds_t>(arguments, context);
  const auto seeding_states = make_host_buffer<dev_seeding_states_t>(arguments, context);
  const auto event_list = make_host_buffer<dev_event_list_t>(arguments, context);

  const auto matched_is_scifi_track_used = make_host_buffer<dev_matched_is_scifi_track_used_t>(arguments, context);

  auto tracks = prepareUnmatchedSeedingTracks(
    first<host_number_of_events_t>(arguments),
    matched_is_scifi_track_used,
    scifi_seed_atomics,
    scifi_seed_hit_number,
    scifi_seed_hits,
    scifi_seeds,
    seeding_states,
    event_list);

  auto& checker = runtime_options.checker_invoker->checker<TrackCheckerSeeding>(name(), m_root_output_filename);
  checker.accumulate(*first<host_mc_events_t>(arguments), tracks, event_list); // FIXME
}
