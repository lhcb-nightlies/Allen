2024-04-03 Allen v4r5
===

This version uses
Rec [v36r5](../../../../Rec/-/tags/v36r5),
Lbcom [v35r5](../../../../Lbcom/-/tags/v35r5),
LHCb [v55r5](../../../../LHCb/-/tags/v55r5),
Detector [v1r30](../../../../Detector/-/tags/v1r30),
Gaudi [v38r1](../../../../Gaudi/-/tags/v38r1) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.04.

This version is released on the `master` branch.
Built relative to Allen [v4r4](/../../tags/v4r4), with the following changes:

### New features ~"new feature"



### Fixes ~"bug fix" ~workaround

- Put Chi2Muon event loop inside the event mask, !1526 (@cagapopo)


### Enhancements ~enhancement



### Code cleanups and changes to tests ~modernisation ~cleanup ~testing



### Documentation ~Documentation


### Other

- Add new sequence for pi0 calib, !1530 (@sargueda)
- Update References for: Allen!1483 based on lhcb-master-mr/11334, !1528 (@lhcbsoft)
- Optimise Monitoring, !1483 (@ahennequ)
